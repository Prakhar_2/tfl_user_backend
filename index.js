import { ApolloServer, gql, makeExecutableSchema } from 'apollo-server-express'
import express from 'express'
import { AuthDirective } from 'graphql-directive-auth'
import { applyMiddleware } from 'graphql-middleware'
import { resolvers, typeDefs } from 'graphql-scalars'
import { graphqlUploadExpress } from 'graphql-upload'
import { createServer } from 'http'
import { merge } from 'lodash'
import config from './src/config/app'
import dataLoaders from './src/data-loaders'
import databaseConnection, { sequelize } from './src/db/models'
import { betDef, betResolvers } from './src/graphql-resources/bet'
import { userDef, userResolvers } from './src/graphql-resources/user'
import { sportDef, sportResolvers } from './src/graphql-resources/sport'
import { commonDef, commonResolvers } from './src/graphql-resources/common'
import onHealthCheck from './src/lib/onHealthCheck'
import redisPubSub from './src/lib/redisPubSub'
import routes from './src/rest-resources/routes'
import cors from 'cors'
import './src/lib/listener'

const schema = gql`
  scalar Upload
  directive @isAuthenticated on FIELD | FIELD_DEFINITION
  directive @isBasicAuthenticated on FIELD | FIELD_DEFINITION

  type Query {
    _empty: String
  }
  type Mutation {
    _empty: String
  }
  type Subscription {
    _empty: String
  }
`
const executableSchema = makeExecutableSchema({
  typeDefs: [schema, commonDef, userDef, sportDef, betDef, betDef, ...typeDefs],
  resolvers: merge({}, commonResolvers, userResolvers, sportResolvers, betResolvers, resolvers),
  schemaDirectives: {
    isAuthenticated: AuthDirective().isAuthenticated
  }
})

async function transactionHandlerMiddleware (resolve, root, args, context, info) {
  if (!root) {
    context.sequelizeTransaction = await sequelize.transaction()
  }

  try {
    const result = await resolve(root, args, context, info)
    if (!root) {
      await context.sequelizeTransaction.commit()
    }
    return result
  } catch (error) {
    if (!root) {
      await context.sequelizeTransaction.rollback()
    }
    throw error
  }
}

const schemaWithMiddleware = applyMiddleware(executableSchema, transactionHandlerMiddleware)

const server = new ApolloServer({
  schema: schemaWithMiddleware,
  context: async ({ req, res, connection }) => {
    const subscriptionContext = connection?.context

    return {
      req,
      res,
      databaseConnection,
      reqTimeStamp: Date.now(),
      ...subscriptionContext,
      pubSub: redisPubSub,
      ...dataLoaders(databaseConnection)
    }
  },
  formatError: (error) => {
    return {
      message: error.message,
      reason: error.originalError && error.originalError.reason,
      locations: error.locations,
      code: error?.extensions?.exception?.code,
      path: error.path,
      extensions: error.extensions
    }
  },
  subscriptions: {
    onConnect: async (_, __, context) => {
      return context
    }
  },
  tracing: config.get('env') !== 'production',
  uploads: false
})
const app = express()

app.use(cors({ origin: config.get('env') === 'production' ? 'https://testnet.betswap.gg' : '*' }))

app.use(graphqlUploadExpress({ maxFileSize: 1000000000, maxFiles: 10 }))

server.applyMiddleware({ app, onHealthCheck })

const httpServer = createServer(app)
server.installSubscriptionHandlers(httpServer)

httpServer.listen({ port: config.get('port') }, () => {
  console.log(`Server ready at port ${config.get('port')}`)
  console.log(`🚀 Subscriptions ready at ws://localhost:${config.get('port')}${server.subscriptionsPath}`)
})

app.use(express.static('./src/public'))

app.use(express.urlencoded({ extended: false }))

app.use(express.json())

app.use('/health-check', async (_, res) => {
  try {
    const response = await onHealthCheck()
    res.json(response)
  } catch (error) {
    res.status(503)
    res.send()
  }
})

app.use(async (req, res, next) => {
  req.sequelize = sequelize
  req.databaseConnection = databaseConnection
  req.pubSub = redisPubSub

  next()
})

app.use(routes)
app.use(async (req, res) => {
  res.status(404).json({ status: 'Not Found' })
})

async function handle (signal) {
  try {
    await sequelize.close()
    await redisPubSub.close()
    console.log(`Received ${signal}`)
  } catch (err) {
    console.log('GraceFull ShutDown Failed', err)
  }
  process.exit(0)
}

process.on('SIGTERM', handle)
process.on('SIGINT', handle)
