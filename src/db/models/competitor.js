module.exports = (sequelize, DataTypes) => {
  const Competitor = sequelize.define('Competitor', {
    id: {
      type: DataTypes.BIGINT,
      primaryKey: true,
      allowNull: false,
      autoIncrement: true
    },
    eventId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      foreignKey: true
    },
    participantId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      foreignKey: true
    },
    qualifier: {
      type: DataTypes.STRING,
      allowNull: false
    },
    createdAt: {
      type: DataTypes.DATE
    },
    updatedAt: {
      type: DataTypes.DATE
    }
  }, {
    sequelize,
    underscored: true,
    tableName: 'competitors',
    schema: 'public',
    timestamps: true
  })

  Competitor.associate = models => {
    Competitor.belongsTo(models.Participant, {
      foreignKey: 'participantId'
    })
    Competitor.belongsTo(models.Event, {
      foreignKey: 'eventId'
    })
  }

  return Competitor
}
