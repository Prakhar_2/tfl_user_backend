module.exports = (sequelize, DataTypes) => {
  const Market = sequelize.define('Market', {
    id: {
      type: DataTypes.BIGINT,
      primaryKey: true,
      allowNull: false,
      autoIncrement: true
    },
    marketId: {
      type: DataTypes.INTEGER
    },
    nameEn: {
      type: DataTypes.STRING,
      allowNull: true
    },
    isDeleted: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    created: {
      type: DataTypes.DATE
    },
    modified: {
      type: DataTypes.DATE
    }
  }, {
    sequelize,
    underscored: true,
    tableName: 'pulls_markets',
    schema: 'public',
    timestamps: false
  })

  Market.associate = models => {
    Market.hasMany(models.Bet, {
      foreignKey: 'marketId'
    })
    Market.hasMany(models.Spread, {
      foreignKey: 'marketId'
    })
  }

  return Market
}
