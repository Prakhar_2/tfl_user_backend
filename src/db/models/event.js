module.exports = (sequelize, DataTypes) => {
  const Event = sequelize.define('Event', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    fixtureId: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    isDeleted: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    sportNameEn: {
      type: DataTypes.STRING,
      allowNull: true
    },
    locationNameEn: {
      type: DataTypes.STRING,
      allowNull: true
    },
    leagueNameEn: {
      type: DataTypes.STRING,
      allowNull: true
    },
    fixtureStatus: {
      type: DataTypes.INTEGER
    },
    startDate: {
      type: DataTypes.STRING,
      allowNull: true
    },
    lastUpdate: {
      type: DataTypes.STRING
    },
    isEventBlacklisted: {
      type: DataTypes.BOOLEAN
    },
    created: {
      type: DataTypes.DATE,
      allowNull: true
    },
    modified: {
      type: DataTypes.DATE
    },
    leagueId: {
      type: DataTypes.BIGINT,
      foreignKey: true
    },
    locationId: {
      type: DataTypes.BIGINT,
      foreignKey: true
    },
    sportId: {
      type: DataTypes.BIGINT,
      foreignKey: true
    }
  }, {
    sequelize,
    underscored: true,
    tableName: 'pulls_events',
    schema: 'public',
    timestamps: false
  })

  Event.associate = models => {
    Event.belongsTo(models.League, {
      foreignKey: 'leagueId'
    })
    Event.belongsTo(models.Location, {
      foreignKey: 'locationId'
    })
    Event.belongsTo(models.Sport, {
      foreignKey: 'sportId'
    })
    Event.hasMany(models.EventParticipant, {
      foreignKey: 'eventId',
      onDelete: 'cascade'
    })
    Event.hasMany(models.Bet, {
      foreignKey: 'eventId',
      onDelete: 'cascade'
    })
    Event.hasMany(models.Spread, {
      foreignKey: 'eventId',
      onDelete: 'cascade'
    })
    Event.belongsToMany(models.Participant, {
      foreignKey: 'eventId',
      through: models.EventParticipant
    })
    Event.hasMany(models.WithdrawRequest, {
      foreignKey: 'fixtureId',
      onDelete: 'cascade'
    })
  }
  return Event
}
