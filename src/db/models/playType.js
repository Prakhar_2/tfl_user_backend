module.exports = (sequelize, DataTypes) => {
  const PlayType = sequelize.define('PlayType', {
    id: {
      type: DataTypes.BIGINT,
      autoIncrement: true,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    createdAt: {
      type: DataTypes.DATE
    },
    updatedAt: {
      type: DataTypes.DATE
    }
  }, {
    sequelize,
    underscored: true,
    tableName: 'play_types',
    schema: 'public',
    timestamps: true
  })

  return PlayType
}
