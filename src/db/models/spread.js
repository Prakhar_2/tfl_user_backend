module.exports = (sequelize, DataTypes) => {
  const Spread = sequelize.define('Spread', {
    id: {
      type: DataTypes.BIGINT,
      primaryKey: true,
      allowNull: false,
      autoIncrement: true
    },
    eventId: {
      type: DataTypes.BIGINT
    },
    marketId: {
      type: DataTypes.BIGINT
    },
    outcomeId: {
      type: DataTypes.STRING
    },
    outcomeName: {
      type: DataTypes.STRING
    },
    laySpread: {
      type: DataTypes.FLOAT,
      defaultValue: 0.0
    },
    backSpread: {
      type: DataTypes.FLOAT,
      defaultValue: 0.0
    },
    createdAt: {
      type: DataTypes.DATE
    },
    updatedAt: {
      type: DataTypes.DATE
    }
  }, {
    sequelize,
    underscored: true,
    tableName: 'spreads',
    schema: 'public',
    timestamps: true
  })

  Spread.associate = models => {
    Spread.belongsTo(models.Event, {
      foreignKey: 'eventId'
    })
    Spread.belongsTo(models.Market, {
      foreignKey: 'marketId'
    })
  }
  return Spread
}
