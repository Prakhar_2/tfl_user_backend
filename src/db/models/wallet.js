module.exports = (sequelize, DataTypes) => {
  const Wallet = sequelize.define('Wallet', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    amount: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0.0
    },
    userId: {
      type: DataTypes.BIGINT,
      foreignKey: true
    },
    userType: {
      type: DataTypes.STRING
    },
    currency: {
      type: DataTypes.INTEGER
    }
  }, {
    sequelize,
    underscored: true,
    tableName: 'wallets',
    schema: 'public',
    timestamps: true
  })

  Wallet.associate = models => {
    Wallet.belongsTo(models.User, {
      foreignKey: 'userId'
    })
    Wallet.hasMany(models.Ledger, {
      foreignKey: 'sourceWalletId',
      onDelete: 'cascade'
    })
    Wallet.hasMany(models.Ledger, {
      foreignKey: 'targetWalletId',
      onDelete: 'cascade'
    })
    Wallet.hasMany(models.WithdrawRequest, {
      foreignKey: 'walletId',
      onDelete: 'cascade'
    })
  }

  return Wallet
}
