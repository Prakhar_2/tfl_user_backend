module.exports = (sequelize, DataTypes) => {
  const BetMatch = sequelize.define('BetMatch', {
    id: {
      type: DataTypes.BIGINT,
      autoIncrement: true,
      primaryKey: true
    },
    agreedAmount: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    agreedOdds: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    betId: {
      type: DataTypes.BIGINT,
      allowNull: true
    },
    matchBetId: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    createdAt: {
      type: DataTypes.DATE
    },
    updatedAt: {
      type: DataTypes.DATE
    }
  }, {
    sequelize,
    underscored: true,
    tableName: 'bet_matches',
    schema: 'public',
    timestamps: true
  })

  BetMatch.associate = models => {
    BetMatch.belongsTo(models.Bet, {
      foreignKey: 'betId'
    })
  }

  return BetMatch
}
