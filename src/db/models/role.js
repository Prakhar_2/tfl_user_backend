module.exports = (sequelize, DataTypes) => {
  const Role = sequelize.define('Role', {
    id: {
      type: DataTypes.BIGINT,
      autoIncrement: true,
      primaryKey: true,
      allowNull: false
    },
    name: {
      type: DataTypes.STRING
    },
    resourceType: {
      type: DataTypes.STRING
    },
    resourceId: {
      type: DataTypes.BIGINT
    },
    createdAt: {
      type: DataTypes.DATE
    },
    updatedAt: {
      type: DataTypes.DATE
    }
  }, {
    sequelize,
    underscored: true,
    tableName: 'roles',
    schema: 'public',
    timestamps: true
  })

  return Role
}
