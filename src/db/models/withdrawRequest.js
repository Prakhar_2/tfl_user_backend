import { WITHDRAW_REQUEST_STATUS } from '../../common/constants'

module.exports = (sequelize, DataTypes) => {
  const WithdrawRequest = sequelize.define('WithdrawRequest', {
    id: {
      autoIncrement: true,
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true
    },
    userId: {
      type: DataTypes.BIGINT,
      allowNull: false
    },
    status: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: WITHDRAW_REQUEST_STATUS.PENDING
    },
    fixtureId: {
      type: DataTypes.BIGINT,
      allowNull: false
    },
    amount: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      defaultValue: 0.0
    },
    systemFee: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      defaultValue: 0.0
    },
    walletId: {
      type: DataTypes.BIGINT,
      allowNull: true
    },
    signature: {
      type: DataTypes.STRING,
      allowNull: true
    },
    timestamp: {
      type: DataTypes.DATE,
      allowNull: true
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false
    }
  }, {
    sequelize,
    underscored: true,
    tableName: 'withdraw_requests',
    schema: 'public',
    timestamps: true
  })

  WithdrawRequest.associate = models => {
    WithdrawRequest.belongsTo(models.User, {
      foreignKey: 'userId'
    })
    WithdrawRequest.belongsTo(models.Event, {
      foreignKey: 'fixtureId',
      targetKey: 'fixtureId',
      onDelete: 'cascade'
    })
    WithdrawRequest.belongsTo(models.Wallet, {
      foreignKey: 'walletId',
      onDelete: 'cascade'
    })
    WithdrawRequest.hasMany(models.Bet, {
      foreignKey: 'withdrawRequestId',
      onDelete: 'cascade'
    })
  }

  return WithdrawRequest
}
