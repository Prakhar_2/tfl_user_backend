import { SETTLEMENT } from '../../common/constants'

module.exports = (sequelize, DataTypes) => {
  const Bet = sequelize.define('Bet', {
    id: {
      type: DataTypes.BIGINT,
      primaryKey: true,
      allowNull: false,
      autoIncrement: true
    },
    betId: {
      type: DataTypes.STRING,
      allowNull: false
    },
    betType: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    isCashedOut: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    profit: {
      type: DataTypes.FLOAT,
      defaultValue: 0.0
    },
    stake: {
      type: DataTypes.DOUBLE,
      allowNull: false
    },
    status: {
      type: DataTypes.INTEGER,
      defaultValue: SETTLEMENT.PENDING
    },
    winnings: {
      type: DataTypes.FLOAT,
      defaultValue: 0.0
    },
    winningsAfterCommission: {
      type: DataTypes.FLOAT,
      defaultValue: 0.0
    },
    userId: {
      type: DataTypes.INTEGER,
      foreignKey: true,
      allowNull: false
    },
    walletId: {
      type: DataTypes.BIGINT,
      foreignKey: true,
      allowNull: false
    },
    outcomeId: {
      type: DataTypes.STRING
    },
    outcomeName: {
      type: DataTypes.STRING
    },
    marketId: {
      type: DataTypes.BIGINT,
      foreignKey: true,
      allowNull: false
    },
    eventId: {
      type: DataTypes.BIGINT,
      foreignKey: true,
      allowNull: false
    },
    slippage: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0
    },
    offeredOdds: {
      type: DataTypes.FLOAT,
      allowNull: true,
      defaultValue: 0
    },
    matchingStatus: {
      type: DataTypes.STRING
    },
    odds: {
      type: DataTypes.FLOAT,
      allowNull: false
    },
    unmatchedLiability: {
      type: DataTypes.FLOAT,
      defaultValue: 0.0
    },
    matchedLiability: {
      type: DataTypes.FLOAT,
      defaultValue: 0.0
    },
    transactionHash: {
      type: DataTypes.STRING,
      allowNull: true,
      unique: true
    },
    withdrawRequestId: {
      type: DataTypes.BIGINT
    },
    createdAt: {
      type: DataTypes.DATE
    },
    updatedAt: {
      type: DataTypes.DATE
    }
  }, {
    sequelize,
    underscored: true,
    tableName: 'bets',
    schema: 'public',
    timestamps: true
  })

  Bet.associate = models => {
    Bet.belongsTo(models.User, {
      foreignKey: 'userId'
    })
    Bet.belongsTo(models.Event, {
      foreignKey: 'eventId'
    })
    Bet.belongsTo(models.Market, {
      foreignKey: 'marketId'
    })
    Bet.hasMany(models.BetMatch, {
      foreignKey: 'betId',
      onDelete: 'cascade'
    })
    Bet.hasMany(models.Ledger, {
      foreignKey: 'betId',
      onDelete: 'cascade'
    })
    Bet.belongsTo(models.WithdrawRequest, {
      foreignKey: 'withdrawRequestId'
    })
  }
  return Bet
}
