import config from '../../config/app'

module.exports = (sequelize, DataTypes) => {
  const Sport = sequelize.define('Sport', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    sportId: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    isDeleted: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    nameEn: {
      type: DataTypes.STRING,
      allowNull: false
    },
    created: {
      type: DataTypes.DATE,
      allowNull: false
    },
    modified: {
      type: DataTypes.DATE
    },
    iconUrl: {
      type: DataTypes.VIRTUAL,
      get () {
        const iconUrl = `${config.get('aws.s3.static_asset_url')}/icons/sports/${this.sportId}`
        return {
          active: `${iconUrl}-b.svg`,
          inactive: `${iconUrl}-g.svg`
        }
      }
    }
  }, {
    sequelize,
    underscored: true,
    tableName: 'pulls_sports',
    schema: 'public',
    timestamps: false
  })

  Sport.associate = models => {
    Sport.hasMany(models.League, {
      foreignKey: 'sportId',
      onDelete: 'cascade'
    })
    Sport.hasMany(models.Event, {
      foreignKey: 'sportId',
      onDelete: 'cascade'
    })
    Sport.belongsToMany(models.Location, {
      foreignKey: 'sportId',
      through: models.League
    })
  }

  return Sport
}
