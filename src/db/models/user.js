const { USER_TYPES } = require('../../common/constants')

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    id: {
      type: DataTypes.BIGINT,
      autoIncrement: true,
      primaryKey: true,
      allowNull: false
    },
    firstName: {
      type: DataTypes.STRING
    },
    lastName: {
      type: DataTypes.STRING
    },
    email: {
      type: DataTypes.STRING
    },
    userName: {
      type: DataTypes.STRING,
      allowNull: false
    },
    blockchainAddress: {
      type: DataTypes.STRING,
      allowNull: false
    },
    active: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true
    },
    imageUrl: {
      type: DataTypes.STRING,
      allowNull: true
    },
    nonce: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: DataTypes.UUIDV4
    },
    createdAt: {
      type: DataTypes.DATE
    },
    updatedAt: {
      type: DataTypes.DATE
    },
    dateOfBirth: {
      type: DataTypes.DATE
    }
  }, {
    sequelize,
    underscored: true,
    tableName: 'users',
    schema: 'public',
    timestamps: true
  })

  User.associate = models => {
    User.hasMany(models.Bet, {
      foreignKey: 'userId',
      onDelete: 'cascade'
    })
    User.hasMany(models.Wallet, {
      scope: {
        userType: USER_TYPES.USER
      },
      foreignKey: 'userId',
      onDelete: 'cascade'
    })
    User.hasMany(models.WithdrawRequest, {
      foreignKey: 'userId',
      onDelete: 'cascade'
    })
  }

  return User
}
