module.exports = (sequelize, DataTypes) => {
  const Participant = sequelize.define('Participant', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    participantId: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    isDeleted: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    nameEn: {
      type: DataTypes.STRING,
      allowNull: false
    },
    created: {
      type: DataTypes.DATE,
      allowNull: false
    },
    modified: {
      type: DataTypes.DATE
    }
  }, {
    sequelize,
    underscored: true,
    tableName: 'pulls_participants',
    schema: 'public',
    timestamps: false
  })

  Participant.associate = models => {
    Participant.hasMany(models.Competitor, {
      foreignKey: 'participantId',
      onDelete: 'cascade'
    })
    Participant.hasMany(models.EventParticipant, {
      foreignKey: 'participantId',
      onDelete: 'cascade'
    })
  }

  return Participant
}
