module.exports = (sequelize, DataTypes) => {
  const League = sequelize.define('League', {
    id: {
      type: DataTypes.BIGINT,
      primaryKey: true,
      allowNull: false,
      autoIncrement: true
    },
    leagueId: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    isDeleted: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    nameEn: {
      type: DataTypes.STRING,
      allowNull: true
    },
    created: {
      type: DataTypes.DATE,
      allowNull: true
    },
    modified: {
      type: DataTypes.DATE
    },
    locationId: {
      type: DataTypes.BIGINT,
      allowNull: true,
      foreignKey: true
    },
    sportId: {
      type: DataTypes.BIGINT,
      allowNull: true,
      foreignKey: true
    },
    season: {
      type: DataTypes.STRING,
      allowNull: true
    }
  }, {
    sequelize,
    underscored: true,
    tableName: 'pulls_leagues',
    schema: 'public',
    timestamps: false
  })

  League.associate = models => {
    League.belongsTo(models.Sport, {
      foreignKey: 'sportId'
    })
    League.belongsTo(models.Location, {
      foreignKey: 'locationId'
    })
    League.hasMany(models.Event, {
      foreignKey: 'leagueId',
      onDelete: 'cascade'
    })
  }

  return League
}
