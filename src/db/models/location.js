module.exports = (sequelize, DataTypes) => {
  const Location = sequelize.define('Location', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    isDeleted: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    nameEn: {
      type: DataTypes.STRING,
      allowNull: true
    },
    created: {
      type: DataTypes.DATE,
      allowNull: true
    },
    modified: {
      type: DataTypes.DATE
    },
    locationId: {
      type: DataTypes.INTEGER
    }
  }, {
    sequelize,
    underscored: true,
    tableName: 'pulls_locations',
    schema: 'public',
    timestamps: false
  })

  Location.associate = models => {
    Location.hasMany(models.League, {
      foreignKey: 'locationId',
      onDelete: 'cascade'
    })
    Location.hasMany(models.Event, {
      foreignKey: 'locationId',
      onDelete: 'cascade'
    })
  }

  return Location
}
