import { TRANSACTION_STATUS } from '../../common/constants'

module.exports = (sequelize, DataTypes) => {
  const Ledger = sequelize.define('Ledger', {
    id: {
      type: DataTypes.BIGINT,
      primaryKey: true,
      allowNull: false,
      autoIncrement: true
    },
    sourceWalletId: {
      type: DataTypes.BIGINT
    },
    targetWalletId: {
      type: DataTypes.BIGINT
    },
    amount: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0.0
    },
    trackingId: {
      type: DataTypes.UUID,
      allowNull: true,
      unique: true,
      defaultValue: DataTypes.UUIDV4
    },
    status: {
      type: DataTypes.INTEGER,
      defaultValue: TRANSACTION_STATUS.PENDING
    },
    comment: {
      type: DataTypes.STRING,
      allowNull: true
    },
    transactionType: {
      type: DataTypes.STRING,
      allowNull: true,
      foreignKey: true
    },
    createdAt: {
      type: DataTypes.DATE
    },
    updatedAt: {
      type: DataTypes.DATE
    },
    betId: {
      type: DataTypes.BIGINT,
      allowNull: true
    }
  }, {
    sequelize,
    underscored: true,
    tableName: 'ledgers',
    schema: 'public',
    timestamps: true
  })

  Ledger.associate = models => {
    Ledger.belongsTo(models.Wallet, {
      foreignKey: 'sourceWalletId',
      as: 'sourceWallet'
    })
    Ledger.belongsTo(models.Wallet, {
      foreignKey: 'targetWalletId',
      as: 'targetWallet'
    })
    Ledger.belongsTo(models.Bet, {
      foreignKey: 'betId'
    })
  }

  return Ledger
}
