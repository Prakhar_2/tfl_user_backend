module.exports = (sequelize, DataTypes) => {
  const TokenLedger = sequelize.define('TokenLedger', {
    id: {
      type: DataTypes.BIGINT,
      primaryKey: true,
      allowNull: false,
      autoIncrement: true
    },
    account: {
      type: DataTypes.STRING
    },
    amount: {
      type: DataTypes.FLOAT
    },
    ipAddress: {
      type: DataTypes.STRING
    },
    createdAt: {
      type: DataTypes.DATE
    },
    updatedAt: {
      type: DataTypes.DATE
    }
  }, {
    sequelize,
    underscored: true,
    tableName: 'token_ledgers',
    schema: 'public',
    timestamps: true
  })

  return TokenLedger
}
