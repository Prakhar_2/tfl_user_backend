module.exports = (sequelize, DataTypes) => {
  const EventParticipant = sequelize.define('EventParticipant', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    participantId: {
      type: DataTypes.BIGINT,
      allowNull: true
    },
    eventId: {
      type: DataTypes.BIGINT,
      allowNull: true
    },
    isDeleted: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    extraData: {
      type: DataTypes.STRING
    },
    created: {
      type: DataTypes.DATE,
      allowNull: false
    },
    modified: {
      type: DataTypes.DATE
    },
    isActive: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    position: {
      type: DataTypes.STRING,
      allowNull: true
    },
    rotationId: {
      type: DataTypes.INTEGER,
      allowNull: true
    }
  }, {
    sequelize,
    underscored: true,
    tableName: 'pulls_eventparticipants',
    schema: 'public',
    timestamps: false
  })

  EventParticipant.associate = models => {
    EventParticipant.belongsTo(models.Event, {
      foreignKey: 'eventId'
    })
    EventParticipant.belongsTo(models.Participant, {
      foreignKey: 'participantId'
    })
  }

  return EventParticipant
}
