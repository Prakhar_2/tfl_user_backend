module.exports = (sequelize, DataTypes) => {
  const Favorite = sequelize.define('Favorite', {
    id: {
      type: DataTypes.BIGINT,
      primaryKey: true,
      allowNull: false,
      autoIncrement: true
    },
    userId: {
      type: DataTypes.INTEGER,
      foreignKey: true,
      allowNull: false
    },
    leagueId: {
      type: DataTypes.BIGINT,
      foreignKey: true,
      allowNull: false
    },
    sportId: {
      type: DataTypes.BIGINT,
      foreignKey: true,
      allowNull: false
    },
    isFavorite: {
      type: DataTypes.BOOLEAN
    },
    createdAt: {
      type: DataTypes.DATE
    },
    updatedAt: {
      type: DataTypes.DATE
    }
  }, {
    sequelize,
    underscored: true,
    tableName: 'favorites',
    schema: 'public',
    timestamps: true
  })

  Favorite.associate = models => {
    Favorite.belongsTo(models.User, {
      foreignKey: 'userId'
    })
    Favorite.belongsTo(models.League, {
      foreignKey: 'leagueId'
    })
    Favorite.belongsTo(models.Sport, {
      foreignKey: 'sportId'
    })
  }
  return Favorite
}
