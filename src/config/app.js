const convict = require('convict')

const config = convict({
  app: {
    name: {
      doc: 'Name of the service',
      format: String,
      default: 'User-Backend-Api'
    }
  },

  env: {
    doc: 'The application environment.',
    format: ['production', 'development', 'staging', 'test'],
    default: 'development',
    env: 'NODE_ENV'
  },

  port: {
    doc: 'The port to bind.',
    format: 'port',
    default: 8080,
    env: 'PORT'
  },

  aws: {
    s3: {
      region: {
        doc: 'S3 region name',
        format: String,
        default: 'us-east-1',
        env: 'AWS_S3_REGION'
      },
      bucket_name: {
        doc: 'bucket name',
        format: String,
        default: '',
        env: 'AWS_S3_BUCKET_NAME'
      },
      access_key_id: {
        doc: 'access key id',
        format: String,
        default: '',
        env: 'AWS_S3_ACCESS_KEY_ID'
      },
      secret_access_key: {
        doc: 'secret access key',
        format: String,
        default: '',
        env: 'AWS_S3_SECRET_ACCESS_KEY'
      },
      static_asset_url: {
        doc: 'The s3 URL to fetch images and other static assets',
        format: String,
        default: 'http://localhost:8080',
        env: 'AWS_S3_STATIC_ASSET_URL'
      }
    }
  },

  auth: {
    app_secret: {
      doc: 'Secret key for token.',
      format: String,
      default: 'notronisca',
      env: 'APP_SECRET'
    },
    token_expiry_time: {
      doc: 'Secret key for token.',
      format: Number,
      default: 60 * 30,
      env: 'AUTH_TOKEN_EXPIRE_TIME'
    }
  },

  pub_sub_redis_db: {
    password: {
      doc: 'Redis Database password',
      format: '*',
      default: '',
      env: 'PUB_SUB_REDIS_DB_PASSWORD'
    },
    host: {
      doc: 'Redis DB host',
      format: String,
      default: '127.0.0.1',
      env: 'PUB_SUB_REDIS_DB_HOST'
    },
    port: {
      doc: 'Redis DB PORT',
      format: 'port',
      default: 6379,
      env: 'PUB_SUB_REDIS_DB_PORT'
    }
  },

  db: {
    name: {
      doc: 'Database Name',
      format: String,
      default: 'api',
      env: 'DB_NAME'
    },
    username: {
      doc: 'Database user',
      format: String,
      default: 'postgres',
      env: 'DB_USERNAME'
    },
    password: {
      doc: 'Database password',
      format: '*',
      default: 'postgres',
      env: 'DB_PASSWORD'
    },
    host: {
      doc: 'DB host',
      format: String,
      default: '127.0.0.1',
      env: 'DB_HOST'
    },
    port: {
      doc: 'DB PORT',
      format: 'port',
      default: '5432',
      env: 'DB_PORT'
    }
  },

  blockchain: {
    smart_contract_address: {
      doc: 'Smart contract address',
      format: String,
      default: '',
      env: 'BLOCKCHAIN_SMART_CONTRACT_ADDRESS'
    },
    confirmation_blocks: {
      doc: 'number of confirmations block',
      format: Number,
      default: 1,
      env: 'BLOCKCHAIN_CONFIRMATION_BLOCKS'
    },
    http_endpoint: {
      doc: 'blockchain RPC end point',
      format: String,
      default: 'http://localhost:8545',
      env: 'BLOCKCHAIN_HTTP_RPC_ENDPOINT'
    },
    redis_db: {
      password: {
        doc: 'Redis Database password',
        format: '*',
        default: '',
        env: 'BLOCKCHAIN_REDIS_DB_PASSWORD'
      },
      host: {
        doc: 'Redis DB host',
        format: String,
        default: '127.0.0.1',
        env: 'BLOCKCHAIN_REDIS_DB_HOST'
      },
      port: {
        doc: 'Redis DB PORT',
        format: 'port',
        default: 6379,
        env: 'BLOCKCHAIN_REDIS_DB_PORT'
      },
      number: {
        doc: 'Redis database index number',
        format: Number,
        default: 0,
        env: 'BLOCKCHAIN_REDIS_DB_NUMBER'
      },
      tls: {
        doc: 'Redis DB TLS',
        format: String,
        default: 'false',
        env: 'BLOCKCHAIN_REDIS_DB_TLS'
      }
    }
  },

  log_level: {
    doc: 'level of logs to show',
    format: String,
    default: 'debug',
    env: 'LOG_LEVEL'
  }
})

config.validate({ allowed: 'strict' })

module.exports = config
