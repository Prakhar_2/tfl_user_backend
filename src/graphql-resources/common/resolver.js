import GetEvent from '../../services/common/getEvent'
import GetSettings from '../../services/common/getSettings'
import requestTestUsdt from '../../services/common/requestTestUsdt'

/**
 *  Common resolver will handle bet related queries and mutation.
 */
export const resolvers = {
  Query: {

    /**
     * This resolver is responsible to fetch all bet related setting
     * @param {Context} context it the context object of gql, contains all the shared info between the resolvers.
     * @returns {SettingResponse}
     */
    GetSettings: async (_, __, context) => {
      const result = await GetSettings.execute(__, context)
      return result.result
    },

    /**
     * This resolver is responsible to fetch all bet related setting
     * @param {Context} context it the context object of gql, contains all the shared info between the resolvers.
     * @returns {EventResponse}
     */
    GetEvent: async (_, { input }, context) => {
      const result = await GetEvent.execute(input, context)
      return result.result
    }
  },

  Mutation: {
    /**
     * This resolver is used to request test USDT
     * @param {Context} context it the context object of gql, contains all the shared info between the resolvers.
     * @returns {RequestTestUsdtResponse}
     */
    RequestTestUsdt: async (_, { input }, context) => {
      const result = await requestTestUsdt.execute(input, context)
      return result.result
    }
  },

  /**
 * Contains all eventResponse  related dataLoaders
 */
  EventResponse: {
    /**
     * This dataLoader is responsible to fetch league response
     * @param {EventResponse}  it contains particular leagueResponse under eventResponse
     * @param {getLeagueByLeagueIdDataLoader} dataLoader it fetch the leagueResponse according to given input
     * @returns {leagueResponse}
     */
    League: async (EventResponse, _, { getLeagueByLeagueIdDataLoader }) => {
      const res = await getLeagueByLeagueIdDataLoader.load(EventResponse.leagueId)
      return res
    },

    /**
    * This dataLoader is responsible to fetch location response
    * @param {EventResponse}  it contains particular locationResponse under eventResponse
    * @param {getLocationByLocationIdDataLoader} dataLoader it fetch the locationResponse according to given input
    * @returns {leagueResponse}
    */
    Location: async (EventResponse, _, { getLocationByLocationIdDataLoader }) => {
      const res = await getLocationByLocationIdDataLoader.load(EventResponse.locationId)
      return res
    },

    /**
    * This dataLoader is responsible to fetch eventParticipant response
    * @param {EventResponse}  it contains particular eventParticipantResponse under eventResponse
    * @param {getEventParticipantsByEventIdDataLoader} dataLoader it fetch the eventParticipantResponse according to given input
    * @returns {eventParticipantResponse}
    */
    EventParticipant: async (EventResponse, _, { getEventParticipantsByEventIdDataLoader }) => {
      const res = await getEventParticipantsByEventIdDataLoader.load(EventResponse.id)
      return res
    }
  },

  /**
* Contains all EventParticipantResponse related dataLoaders
*/
  EventParticipantResponse: {
    /**
    * This dataLoader is responsible to fetch participant response
    * @param {EventParticipantResponse}  it contains particular  participantResponse under EventParticipantResponse
    * @param {getParticipantsByParticipantIdDataLoader} dataLoader it fetch the Participants Response according to given input
    * @returns {participantResponse}
    */
    Participant: async (EventParticipantResponse, _, { getParticipantsByParticipantIdDataLoader }) => {
      const res = await getParticipantsByParticipantIdDataLoader.load(EventParticipantResponse.participantId)
      return res
    }
  }
}
