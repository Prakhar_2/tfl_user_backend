import { gql } from 'apollo-server'

export const typeDef = gql`

  """
  contains all the fields related to location
  """
  type LocationResponse {
    id: String!
    nameEn: String!
    locationId: Int!
    isDeleted: Boolean!
    created: DateTime!
    modified: DateTime!
  }

  """
  contains all the fields related to market
  """
  type MarketResponse {
    id: Int!
    marketId: Int!
    nameEn: String!
    isDeleted: Int
    created: DateTime!
    modified: DateTime!
  }

  """
  contains all the fields related to league
  """
  type LeagueResponse {
    id: String!
    nameEn: String!
    isDeleted: Boolean!
    season: String
    created: DateTime!
    modified: DateTime!
    leagueId: Int!
    locationId: String!
    sportId: String!
  }

  """
  contains the key value of the setting
  """
  type SettingResponse {
    key: String
    value: String
    createdAt: DateTime
    updatedAt: DateTime
  }

  """
  contains the setting response
  """
  type GetSettingsResponse {
    Settings: [SettingResponse]!
  }

  """
  contains all the fields related to event
  """
  type EventResponse {
    id: Int!
    fixtureId: Int!
    sportNameEn: String!
    locationNameEn: String!
    fixtureStatus: String!
    startDate: String!
    lastUpdate: String!
    isEventBlacklisted: Boolean!
    created: DateTime!
    modified: DateTime!
    leagueId: String!
    locationId: String!
    sportId: String!
    League: LeagueResponse!
    Location: LocationResponse!
    EventParticipant: [EventParticipantResponse]
  }

  """
  contains all the fields related to eventParticipant
  """
  type EventParticipantResponse {
    id: Int!
    participantId: Int!
    eventId: Int!
    isDeleted: Boolean!
    created: DateTime!
    modified: DateTime!
    isActive: Boolean
    position: String!
    extraData: String
    rotationId: Int
    Participant: ParticipantResponse
  }

  """
  contains all the fields related to participant
  """
  type ParticipantResponse {
    id: Int!
    participantId: Int!
    nameEn: String!
    isDeleted: Boolean!
    created: DateTime!
    modified: DateTime!
  }

  """
  contains the response of RequestTestUsdt API
  """
  type RequestTestUsdtResponse{
    success: Boolean!
  }

  """
  contains all the fields related to event
  """
  input GetEventInput {
    fixtureId: Int!
  }

  """
  contains all the field required to req test USDT
  """
  input RequestTestUsdtInput {
    blockChainAddress: String!
  }
  extend type Query {
    """
    fetches all the settings details
    """
    GetSettings: GetSettingsResponse!
    """
    fetches the event using the fixture ID
    """
    GetEvent(input: GetEventInput!): EventResponse!
  }

  extend type Mutation {
    """
    Used to request for test USDT
    """
    RequestTestUsdt(input: RequestTestUsdtInput!): RequestTestUsdtResponse!
  }

`
