import CancelBet from '../../services/bet/cancelBet'
import GetBets from '../../services/bet/getBets'
import GetFixtureMatchedBetAmount from '../../services/bet/getFixtureMatchedBetAmount'
import OrderBook from '../../services/bet/orderBook'
import GetFixtureMarketVolume from '../../services/bet/getFixtureMarketVolume'
import PlaceBet from '../../services/bet/placeBet'
import { SUBSCRIPTION_CHANNEL } from '../../common/constants'
import GetAllBets from '../../services/bet/getAllBets'

/**
 *  Bet resolver will handle bet related queries and mutation.
 */
export const resolvers = {

  /**
   * All the queries related to the bet are present in this query resolver
   */
  Query: {

    /**
     * This resolver is responsible to fetch all bet's based on a filter value
     * @param {GetBetInput} input it contains an array of key-value pair to filter bets
     * based on enum value {BetFilterEnum}
     * @param {Context} context it the context object of gql, contains all the shared info between the resolvers.
     * @returns {BetResponse}
     */
    GetBets: async (_, { input }, context) => {
      const result = await GetBets.execute(input, context)
      return result.result
    },

    /**
    * This resolver is responsible to generate order book
    * @param {OrderBookInput} input it contains an key-value pair of required inputs
    * @param {Context} context it the context object of gql, contains all the shared info between the resolvers.
    * @returns {OrderBookResponse}
    */
    OrderBook: async (_, { input }, context) => {
      const result = await OrderBook.execute(input, context)
      return result.result
    },

    /**
    * This resolver is responsible to get the fixture
    * @param {CheckUserAddressInput} input it contains blockchain address of existence user
    * @param {Context} context it the context object of gql, contains all the shared info between the resolvers.
    * @returns {CheckUserAddressResponse}
    */
    GetFixtureMatchedBetAmount: async (_, { input }, context) => {
      const result = await GetFixtureMatchedBetAmount.execute(input, context)
      return result.result
    },

    /**
    * This resolver is responsible to get the fixture
    * @param {GetFixtureMarketVolumeInput} input it contains fixture and market id
    * @param {Context} context it the context object of gql, contains all the shared info between the resolvers.
    * @returns {GetFixtureMarketVolumeResponse}
    */
    GetFixtureMarketVolume: async (_, { input }, context) => {
      const result = await GetFixtureMarketVolume.execute(input, context)
      return result.result
    },

    /**
    * This resolver is responsible to get all the bets
    * @param {GetAllBets} input it contains fixture and market id
    * @param {Context} context it the context object of gql, contains all the shared info between the resolvers.
    * @returns {GetBetsResponse}
    */
    GetAllBets: async (_, { input }, context) => {
      const result = await GetAllBets.execute(input, context)
      return result.result
    }
  },

  /**
   * all the mutations related to the bet are present in mutation resolver
   */
  Mutation: {
    /**
     * This resolver is responsible to cancel the bet according to bet-id
     * @param {CancelBetInput} input it contains an bet-id
     * @param {Context} context it the context object of gql, contains all the shared info between the resolvers.
     * @returns {BetResponse}
     */
    CancelBet: async (_, { input }, context) => {
      const result = await CancelBet.execute(input, context)
      return result.result
    },

    /**
     * This resolver is responsible to place the bet according to fixture-id
     * @param {PlaceBetInput} input it contains an fixture-id
     * @param {Context} context it the context object of gql, contains all the shared info between the resolvers.
     * @returns {BetResponse}
     */
    PlaceBet: async (_, { input }, context) => {
      const result = await PlaceBet.execute(input, context)
      return result.result
    }
  },

  /**
   * All the subscription related to the bet are present in mutation resolver
   */
  Subscription: {
    SportsWiseFixtureMatchedVolumeData: {
      subscribe: (_, __, { pubSub }) => {
        return pubSub.asyncIterator(SUBSCRIPTION_CHANNEL.SPORTS_WISE_FIXTURE_MATCHED_VOLUME_DATA)
      }
    },
    OrderBookSubscription: {
      subscribe: (_, { input }, { pubSub }) => {
        return pubSub.asyncIterator([SUBSCRIPTION_CHANNEL.ORDER_BOOK_SUBSCRIPTION, input.fixtureId, input.marketId].join('_'))
      }
    },
    GetBetSubscription: {
      subscribe: (_, __, { pubSub }) => {
        return pubSub.asyncIterator(SUBSCRIPTION_CHANNEL.GET_BET_SUBSCRIPTION)
      }
    }
  },
  /**
  * Contains all BetResponse  related dataLoaders
  */
  BetResponse: {
    /**
 * This dataLoader is responsible to fetch user response
 * @param {BetResponse}  it contains particular userResponse under betResponse
 * @param {getUserByUserIdDataLoader} dataLoader it fetch the userResponse according to given input
 * @returns {userResponse}
 */
    User: async (BetResponse, _, { getUserByUserIdDataLoader }) => {
      const result = await getUserByUserIdDataLoader.load(BetResponse.userId)
      return result
    },

    /**
    * This resolver is responsible to fetch market response
    * @param {BetResponse}  it contains particular marketResponse under betResponse
    * @param {getMarketByMarketIdDataLoader} dataLoader it fetch the marketResponse according to given input
    * @returns {marketResponse}
    */
    Market: async (BetResponse, _, { getMarketByMarketIdDataLoader }) => {
      if (BetResponse.marketId) {
        const result = await getMarketByMarketIdDataLoader.load(BetResponse.marketId)
        return result
      }
    },

    /**
    * This resolver is responsible to fetch event response
    * @param {BetResponse}  it contains particular eventResponse under betResponse
    * @param {getEventByEventIdDataLoader} dataLoader it fetch the eventResponse according to given input
    * @returns {eventResponse}
    */
    Event: async (BetResponse, _, { getEventByEventIdDataLoader }) => {
      if (BetResponse.eventId) {
        const result = await getEventByEventIdDataLoader.load(BetResponse.eventId)
        return result
      }
    },

    /**
    * This resolver is responsible to fetch wallet response
    * @param {BetResponse}  it contains particular walletResponse under betResponse
    * @param {getWalletByWalletIdDataLoader} dataLoader it fetch the walletResponse according to given input
    * @returns {walletResponse}
    */
    Wallet: async (BetResponse, _, { getWalletByWalletIdDataLoader }) => {
      const result = await getWalletByWalletIdDataLoader.load(BetResponse.walletId)
      return result
    },

    /**
    * This resolver is responsible to fetch withdraw request
    * @param {BetResponse}  it contains particular walletResponse under betResponse
    * @param {getWithdrawRequestByWithdrawRequestId} dataLoader it fetch the walletResponse according to given input
    * @returns {WithdrawResponse}
    */
    WithdrawRequest: async (BetResponse, _, { getWithdrawRequestByWithdrawRequestId }) => {
      if (BetResponse.withdrawRequestId) {
        const result = await getWithdrawRequestByWithdrawRequestId.load(BetResponse.withdrawRequestId)
        return result
      }
    }
  }
}
