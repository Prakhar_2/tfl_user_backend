import { gql } from 'apollo-server'

export const typeDef = gql`
  """
  represents the type of filterType in GetBetFilterInput and BetSorterInput input
  """
  enum BetFilterEnum {
    betType
    status
    matchingStatus
    startDate
    endDate
  }

  """
  represents the type of filterInput in GetBetsInput input
  """
  input GetBetFilterInput {
    key: BetFilterEnum!
    value: String!
  }

  """
  represents the input type of GetBets query
  """
  input GetBetsInput {
    limit: Int
    pageNumber: Int
    order: SortingInput
    filter: [GetBetFilterInput]
  }

  """
  represents the input type of PlaceBet query
  """
  input PlaceBetInput {
    fixtureId: Int
    marketId: Int
    odds: Float
    stake: Float
    outcomeName: String
    outcomeId: String
  }

  """
  represents the input type of PlaceBet query
  """
  input GetPlaceBetInput {
    fixtureId: Int!
    marketId: Int!
    userId: String!
  }

  """
  contains the Bet cancel input
  """
  input CancelBetInput {
    betId: Int!
  }

  """
  represent the input type of OrderBook
  """
  input OrderBookInput {
    marketId: Int,
    fixtureId: Int
  }

  """
  represents the input of GetFixtureMatchedBetAmount
  """
  input GetFixtureMatchedBetAmountInput {
    fixtureIds: [Int!]!
  }

  """
  Input to get filtered sport
  """
  input GetFixtureMarketVolumeFilterInput {
    marketId: Int!
    fixtureId: Int!
  }

  """
  represents the input of GetFixtureMarketBetAmount
  """
  input GetFixtureMarketVolumeInput {
    filter: [GetFixtureMarketVolumeFilterInput!]!
  }

  type BetApprovalResponse {
    signature: String
    outcomeHash: String
  }

  """
  contains all the fields related to bet
  """
  type BetResponse {
    id: Int
    betType: Int
    betId: String
    isCashedOut: Boolean
    profit: Float
    stake: Float
    status: Int
    winnings: Float
    userId: Int
    walletId: Int
    outcomeId: String
    outcomeName: String
    marketId: Int
    eventId: Int
    slippage: Float
    offeredOdds: Float
    winningsAfterCommission: Float
    matchingStatus: String
    odds: String
    unmatchedLiability: Float
    matchedLiability: Float
    createdAt: DateTime
    updatedAt: DateTime
    Market: MarketResponse
    Event: EventResponse
    Wallet: WalletResponse
    User: UserResponse
    WithdrawRequest: WithdrawResponse
    transactionHash: String
  }

  """
  contains all the fields related to bet
  """
  type GetBetsResponse {
    bets: [BetResponse]
    queriedCount: Int
    numberOfPages: Int
  }

  """
  contains all the fields related to order book
  """
  type OrderBookType {
    layBetsUnmatched: Float
    layBetsMatched: Float
    odds: Float,
    backBetsMatched: Float,
    backBetsUnmatched: Float
  }

  """
  contains all the fields related to place bet
  """
  type PlaceBetResponse {
    success: Boolean!
  }

  type GetFixtureMatchedBetAmountResponse {
    outcomeName: String!
    outcomeId: String!
    marketId: String!
    eventId: String!
    totalMatchedLiability: Float
    Event: EventResponse!
    Market: MarketResponse!
  }

  type GetFixtureMarketVolumeResponse {
    eventId: String!
    marketId: String!
    outcomeId: String!
    outcomeName: String!
    betType: Int!
    odds: Float!
    totalMatchedLiability: Float!
    totalUnmatchedLiability: Float!
    Event: EventResponse!
    Market: MarketResponse!
  }

  extend type Query {
    """
    fetches all the bet details
    """
    GetBets(input: GetBetsInput!): GetBetsResponse! @isAuthenticated

    """
    fetches the order Book
    """
    OrderBook(input: OrderBookInput!): JSON!
    """
    fetches the fixture matched bet amount
    """
    GetFixtureMatchedBetAmount(input: GetFixtureMatchedBetAmountInput!): [GetFixtureMatchedBetAmountResponse!]!
    """
    fetches the fixture's market's matched and unmatched bet amount group by outcome and bet-type
    """
    GetFixtureMarketVolume(input: GetFixtureMarketVolumeInput!): [GetFixtureMarketVolumeResponse!]!
    """
    fetches all the bet regardless of user
    """
    GetAllBets(input: GetBetsInput!): [BetResponse]!
  },

  extend type Mutation {
    """
    Cancel the bet
    """
    CancelBet(input: CancelBetInput!): BetResponse! @isAuthenticated

    """
    Place the bet
    """
    PlaceBet(input: PlaceBetInput!): BetApprovalResponse! @isAuthenticated
  },

  extend type Subscription {
    """
    Produces total matched volume data for a sport
    """
    SportsWiseFixtureMatchedVolumeData: JSON!
    """
    Order book subscription
    """
    OrderBookSubscription(input: OrderBookInput!): JSON!
    """
    Provides the bet placed in the subscription
    """
    GetBetSubscription: BetResponse!
  }
`
