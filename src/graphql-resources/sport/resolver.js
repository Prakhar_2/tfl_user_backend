import GetSport from '../../services/sport/getSport'
import GetAllSports from '../../services/sport/getAllSports'
import GetFavoriteLeague from '../../services/sport/getFavoriteLeague'
import SetOrRemoveLeagueAsFavorite from '../../services/sport/setOrRemoveLeagueAsFavorite'

/**
 *  Sport resolver will handle bet related queries and mutation.
 */
export const resolvers = {

  /**
  * All the queries related to the sport are present in this query resolver
  */
  Query: {

    /**
     * This resolver is responsible to fetch all sports and related locations as well
     * @param {Context} context it the context object of gql, contains all the shared info between the resolvers.
     * @returns {SportResponse}
     */
    GetAllSports: async (_, { input }, context) => {
      const result = await GetAllSports.execute(input, context)
      return result.result
    },

    /**
     * This resolver is responsible to fetch sport and related locations as well accoring to given filter
     * @param {Context} context it the context object of gql, contains all the shared info between the resolvers.
     * @returns {SportResponse}
     */
    GetSport: async (_, { input }, context) => {
      const result = await GetSport.execute(input, context)
      return result.result
    },

    /**
     * This resolver is responsible to fetch the user's favorite league
     * @param {Context} context it the context object of gql, contains all the shared info between the resolvers.
     * @returns {GetFavoriteLeagueResponse}
     */
    GetFavoriteLeague: async (_, { input }, context) => {
      const result = await GetFavoriteLeague.execute(input, context)
      return result.result
    }
  },

  Mutation: {
    /**
     * This resolver is responsible to set league as favorite
     * @param {Context} context it the context object of gql, contains all the shared info between the resolvers.
     * @returns {SetOrRemoveLeagueAsFavoriteResponse}
     */
    SetOrRemoveLeagueAsFavorite: async (_, { input }, context) => {
      const result = await SetOrRemoveLeagueAsFavorite.execute(input, context)
      return result.result
    }
  },

  /**
 * Contains all sportResponse  related dataLoaders
 */
  SportResponse: {
    /**
    * This dataLoader is responsible to fetch location response
    * @param {SportResponse}  it contains particular userResponse under  sportResponse
    * @param {getLocationBySportIdDataLoader} dataLoader it fetch the locationResponse according to given input
    * @returns {locationResponse}
    */
    Locations: async (SportResponse, _, { getLocationBySportIdDataLoader }) => {
      return getLocationBySportIdDataLoader.load(SportResponse.id)
    },

    /**
   * This dataLoader is responsible to fetch league response
   * @param {SportResponse}  it contains particular userResponse under  sportResponse
   * @param {getLeagueBySportIdDataLoader} dataLoader it fetch the leagueResponse according to given input
   * @returns {leagueResponse}
   */
    Leagues: async (SportResponse, _, { getLeagueBySportIdDataLoader }) => {
      return getLeagueBySportIdDataLoader.load(SportResponse.id)
    }
  }
}
