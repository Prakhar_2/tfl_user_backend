import { gql } from 'apollo-server'

export const typeDef = gql`
  """
  represents the type of filterType in GetSportFilterInput input
  """
  enum SportFilterEnum {
    id
    sportId
    nameEn
  }

  """
  Input to get filtered sport
  """
  input GetSportFilterInput {
    key: SportFilterEnum!
    value: String!
  }

  """
  Input to get sorting Order
  """
  input SortingInput {
    id: String!
    desc: Boolean!
  }

  """
  Input to get ordered data
  """
  input GetAllSportsInput {
    limit: Int
    pageNumber: Int
    order: SortingInput
    filter: [GetSportFilterInput]
  }

  """
  Input to get favorite league list
  """
  input GetFavoriteLeagueInput {
    sportId: Int!
  }

  """
  Input to create or update favorite league for a user
  """
  input SetOrRemoveLeagueAsFavoriteInput {
    sportId: Int!
    leagueId: Int!
    isFavorite: Boolean!
  }

  """
  contains all the fields related to iconUrl
  """
  type iconUrlResponse {
    active: String!
    inactive: String!
  }

  """
  contains all the fields related to sport
  """
  type SportResponse {
    id: String!
    sportId: Int!
    nameEn: String!
    isDeleted: Boolean!
    created: DateTime!
    modified: DateTime!
    iconUrl: iconUrlResponse!
    Locations: [LocationResponse]!
    Leagues: [LeagueResponse]!
  }

  """
  contains the detail for page and sports response
  """
  type GetAllSportsResponse {
    Sports: [SportResponse]
    queriedCount: Int
    numberOfPages: Int
  }

  """
  contains the detail if the league is favorite or not
  """
  type GetFavoriteLeagueResponse {
    id: Int!,
    userId: Int!,
    leagueId: Int!,
    isFavorite: Boolean!,
    createdAt: DateTime!,
    updatedAt: DateTime!
    League: LeagueResponse!
  }

  """
  contains the response of SetOrRemoveLeagueAsFavorite API
  """
  type SetOrRemoveLeagueAsFavoriteResponse {
    success: Boolean!
  }

  extend type Query {
    """
    fetches the sport and respective location leagues based on filter
    """
    GetSport(input: GetSportFilterInput!): SportResponse!
    """
    fetches all the sports and respective location leagues
    """
    GetAllSports(input: GetAllSportsInput!): GetAllSportsResponse!
    """
    fetches all the favorite leagues of a sport for a user
    """
    GetFavoriteLeague(input: GetFavoriteLeagueInput!): [GetFavoriteLeagueResponse] @isAuthenticated
  }

  extend type Mutation {
    """
    Update or create league as favorite for the user of a sport
    """
    SetOrRemoveLeagueAsFavorite(input: SetOrRemoveLeagueAsFavoriteInput!): SetOrRemoveLeagueAsFavoriteResponse @isAuthenticated
  }
`
