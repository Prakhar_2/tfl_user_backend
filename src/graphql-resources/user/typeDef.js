import { gql } from 'apollo-server'

export const typeDef = gql`
  """
  represents LoginInput input
  """
  input LoginInput {
    signedMessage: String!
    blockchainAddress: String!
  }

  """
  represents UserUpdateInput input
  """
  input UserUpdateInput {
    firstName: String
    lastName: String
    email: String
    userName: String
    dateOfBirth: String
  }

  """
  represents CheckUserAddress input
  """
  input CheckUserAddressInput {
    userAddress: String!
  }

  """
  represent WithdrawApproval input
  """
  input WithdrawApprovalInput {
    fixtureId: String!
  }

  """
  represent WithdrawSignature input
  """
  input GetWithdrawDetailInput {
    fixtureId: String!
  }

  """
  represent GetFixtureWithdrawAmount subscription input
  """
  input GetFixtureWithdrawAmountInput {
    fixtureId: Int!
    userId: Int!
  }

  """
  contains all the field related to fixture list
  """
  input GetFixtureListInput{
    limit: Int
    pageNumber: Int
  }

  """
  contains all the fields related to wallet
  """
  type WalletResponse {
    id: Int!
    amount: Float!
    userId: Int!
    userType: String!
    currency: Int!
    createdAt: DateTime!
    updatedAt: DateTime!
  }

  """
  contains all the fields related to user
  """
  type UserResponse {
    id: Int!
    firstName: String
    lastName: String
    userName: String!
    email: String
    blockchainAddress: String!
    active: Boolean!
    imageUrl: String
    createdAt: DateTime!
    updatedAt: DateTime!
    dateOfBirth: Date
    Wallet: WalletResponse!
  }

  """
  contains all the fields related to user
  """
  type GetUserResponse {
    User: UserResponse!
    maxBetWinning: Float!
    totalBetWinning: Float!
    totalNumberOfBets: Int!
  }

  """
  contains access-token and user related fields
  """
  type LoginResponse {
    accessToken: String!
    User: UserResponse!
  }

  """
  contains status of user existence
  """
  type DeactivateUserResponse {
    success: Boolean!
  }

  """
  contains user address related field
  """
  type CheckUserAddressResponse {
    message: String!
    exist: Boolean!
    nonce: String!
  }

  """
  contains all the fields related to the uploaded file
  """
  type UploadProfilePictureResponse {
    uploaded: Boolean!
    location: String
  }

  """
  contains the withdraw request response
  """
  type WithdrawResponse {
    id: Int!
    userId: Int!
    fixtureId: Int!
    walletId: Int!
    signature: String
    amount: Float!
    systemFee: Float!
    status: String!
    timestamp: DateTime
    createdAt: DateTime!
    updatedAt: DateTime!
    User: UserResponse!
    Event: EventResponse!
    Wallet: WalletResponse!
  }

  """
  contains the fixture withdraw amount response
  """
  type GetFixtureWithdrawAmountResponse {
    maxAmount: String
  }

  """
  contains the fixtureList response
  """
  type FixtureResponse {
    userId: String!
    eventId: String
    WithdrawRequest: WithdrawResponse
    Event: EventResponse
  }
  """
  contains the GetFixtureListResponse
  """
  type GetFixtureListResponse{
    numberOfPages: Int!
    queriedCount: Int!
    bets: [FixtureResponse]
  }
  """
  contains the withdrawable amount for a fixture response
  """
  type GetWithdrawableAmountForAFixtureResponse {
    betIds: [Int]!
    userId: Int!
    fixtureId: Int!
    withdrawableAmount: Float!
  }

  extend type Query {
    """
    fetches all the  Authenticated User details
    """
    GetUser: GetUserResponse! @isAuthenticated

    """
    fetches all the  fixtureList details
    """
    GetFixtureList(input: GetFixtureListInput!): GetFixtureListResponse! @isAuthenticated

    """
    check the user address existence
    """
    CheckUserAddress(input: CheckUserAddressInput!): CheckUserAddressResponse!

    """
    fetches the approved withdraw request
    """
    GetWithdrawSignature(input: GetWithdrawDetailInput!): WithdrawResponse!  @isAuthenticated

    """
    fetches the amount for the user can withdraw
    """
    GetWithdrawableAmountForAFixture(input: GetWithdrawDetailInput!): GetWithdrawableAmountForAFixtureResponse!  @isAuthenticated
  }

  input GenerateWithdrawSignatureForCancelledBetInput {
    betId: Int!
  }

  extend type Mutation {
    """
    creates user of given login input
    """
    Login(input: LoginInput!): LoginResponse!

    """
    update user for a given  input
    """
    UpdateUser(input: UserUpdateInput!): UserResponse! @isAuthenticated

    """
    Deactivate the  authenticated user
    """
    Deactivate: DeactivateUserResponse! @isAuthenticated

    """
    To upload profile picture of a user
    """
    UploadProfilePicture(file: Upload!): UploadProfilePictureResponse! @isAuthenticated

    """
    Used to withdraw the bet amount
    """
    WithdrawApproval(input: WithdrawApprovalInput): WithdrawResponse! @isAuthenticated

    GenerateWithdrawSignatureForCancelledBet(input: GenerateWithdrawSignatureForCancelledBetInput!): WithdrawResponse @isAuthenticated
  }
`
