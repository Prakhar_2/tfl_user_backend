import CheckUserAddress from '../../services/user/checkUserAddress'
import Deactivate from '../../services/user/deactivate'
import GenerateWithdrawSignatureForCancelledBet from '../../services/user/generateWithdrawSignatureForCancelledBet'
import GetFixtureList from '../../services/user/getFixtureList'
import GetUser from '../../services/user/getUser'
import GetWithdrawableAmountForAFixture from '../../services/user/getWithdrawableAmountForAFixture'
import GetWithdrawSignature from '../../services/user/getWithdrawSignature'
import Login from '../../services/user/login'
import UpdateUser from '../../services/user/updateUser'
import UploadProfilePicture from '../../services/user/uploadProfilePicture'
import WithdrawApproval from '../../services/user/withdrawApproval'

/**
 *  User resolver will handle user related queries and mutation.
 */
export const resolvers = {

  /**
 * All the queries related to the user are present in this query resolver
 */
  Query: {

    /**
   * This resolver is responsible to fetch the authenticated user
   * @param {Context} context it the context object of gql, contains all the shared info between the resolvers.
   * @returns {GetUserResponse}
   */
    GetUser: async (_, __, context) => {
      const result = await GetUser.execute(__, context)
      return result.result
    },

    /**
  * This resolver is responsible to check the authenticated user address existence
  * @param {CheckUserAddressInput} input it contains blockchain address of existence user
  * @param {Context} context it the context object of gql, contains all the shared info between the resolvers.
  * @returns {CheckUserAddressResponse}
  */
    CheckUserAddress: async (_, { input }, context) => {
      const result = await CheckUserAddress.execute(input, context)
      return result.result
    },

    /**
    * This resolver is responsible to get withdraw signature
    * @param {GetWithdrawDetailInput} input it contains fixture id of existence user
    * @param {Context} context it the context object of gql, contains all the shared info between the resolvers.
    * @returns {WithdrawResponse}
    */
    GetWithdrawSignature: async (_, { input }, context) => {
      const result = await GetWithdrawSignature.execute(input, context)
      return result.result
    },

    /**
    * This resolver is responsible to get fixture list signature
    * @param {Context} context it the context object of gql, contains all the shared info between the resolvers.
    * @returns {GetFixtureListResponse}
    */
    GetFixtureList: async (_, { input }, context) => {
      const result = await GetFixtureList.execute(input, context)
      return result.result
    },

    /**
    * This resolver is responsible to get withdraw signature
    * @param {GetWithdrawDetailInput} input it contains fixture id of existence user
    * @param {Context} context it the context object of gql, contains all the shared info between the resolvers.
    * @returns {GetWithdrawableAmountForAFixtureResponse}
    */
    GetWithdrawableAmountForAFixture: async (_, { input }, context) => {
      const result = await GetWithdrawableAmountForAFixture.execute(input, context)
      return result.result
    }
  },

  Mutation: {

    /**
     * This resolver is responsible for the creation of new user if it does not exist
     * @param {LoginInput} input it contains blockchain address of the new user.
     * @param {Context} context it the context object of gql, contains all the shared info between the resolvers.
     * @returns {LoginResponse}
     */
    Login: async (_, { input }, context) => {
      const result = await Login.execute(input, context)
      return result.result
    },

    /**
     * This resolver is responsible for the deactivation of user if it exist
     * @param {Context} context it the context object of gql, contains all the shared info between the resolvers.
     * @returns {DeactivateUserResponse}
     */
    Deactivate: async (_, { input }, context) => {
      const result = await Deactivate.execute(input, context)
      return result.result
    },

    /**
     * This resolver is responsible for the update the authenticated  user if it exist
     * @param {Context} context it the context object of gql, contains all the shared info between the resolvers.
     * @returns {GetUserResponse}
     */
    UpdateUser: async (_, { input }, context) => {
      const result = await UpdateUser.execute(input, context)
      return result.result
    },

    /**
     * This resolver is responsible for the deactivation of user if it exist
     * @param {file} input it contain file to to upload
     * @param {Context} context it the context object of gql, contains all the shared info between the resolvers.
     * @returns {UploadProfilePictureResponse}
     */
    UploadProfilePicture: async (_, file, context) => {
      const result = await UploadProfilePicture.execute(file, context)
      if (result.failed) {
        throw result.errors
      }
      return result.result
    },

    /**
     * This resolver is responsible to create a withdraw request and verify it using redis
     * @param {WithdrawApprovalInput} input it contains the userId fixtureId and user's blockchain address
     * based on enum value {BetFilterEnum}
     * @param {Context} context it the context object of gql, contains all the shared info between the resolvers.
     * @returns {WithdrawApprovalResponse}
     */
    WithdrawApproval: async (_, { input }, context) => {
      const result = await WithdrawApproval.execute(input, context)
      return result.result
    },

    GenerateWithdrawSignatureForCancelledBet: async (_, { input }, context) => {
      const result = await GenerateWithdrawSignatureForCancelledBet.execute(input, context)
      return result.result
    }
  },

  /**
  * Contains all UserResponse  related dataLoaders
  */
  UserResponse: {
    /**
    * This dataLoader is responsible to fetch user's wallet response
    * @param {UserResponse}  it contains particular walletResponse under UserResponse
    * @param {getUserWalletByUserIdDataLoader} dataLoader it fetch the walletResponse  according to given input
    * @returns {walletResponse}
    */
    Wallet: async (UserResponse, _, { getUserWalletByUserIdDataLoader }) => {
      return getUserWalletByUserIdDataLoader.load(UserResponse.id)
    }
  },

  // Subscription: {
  //   GetWithdrawMultisig: {
  //     subscribe: (_, { input }, { pubSub }) => {
  //       const { userId, fixtureId } = input
  //       return pubSub.asyncIterator([SUBSCRIPTION_CHANNEL.GET_WITHDRAW_MULTISIG, userId, fixtureId].join('_'))
  //     }
  //   }
  // },

  /**
* Contains all withdrawResponse  related dataLoaders
*/
  WithdrawResponse: {
    /**
  * This dataLoader is responsible to fetch user's response of WithdrawResponse
  * @param {WithdrawResponse}  it contains particular userResponse under WithdrawResponse
  * @param {getUserByUserIdDataLoader} dataLoader it fetch the walletResponse  according to given input
  * @returns {userResponse}
  */
    User: async (WithdrawResponse, _, { getUserByUserIdDataLoader }) => {
      const result = await getUserByUserIdDataLoader.load(WithdrawResponse.userId)
      return result
    },
    /**
   * This dataLoader is responsible to fetch user's response of WithdrawResponse
   * @param {WithdrawResponse}  it contains particular eventResponse under WithdrawResponse
   * @param {getEventByFixtureIdDataLoader} dataLoader it fetch the eventResponse  according to given input
   * @returns {eventResponse}
  */
    Event: async (WithdrawResponse, _, { getEventByFixtureIdDataLoader }) => {
      const result = await getEventByFixtureIdDataLoader.load(WithdrawResponse.fixtureId)
      return result
    },

    /**
  * This dataLoader is responsible to fetch wallet's response of WithdrawResponse
  * @param {WithdrawResponse}  it contains particular eventResponse under WithdrawResponse
  * @param {getWalletByWalletIdDataLoader} dataLoader it fetch the walletResponse  according to given input
  * @returns {walletResponse}
  */
    Wallet: async (WithdrawResponse, _, { getWalletByWalletIdDataLoader }) => {
      const result = await getWalletByWalletIdDataLoader.load(WithdrawResponse.walletId)
      return result
    }
  }
}
