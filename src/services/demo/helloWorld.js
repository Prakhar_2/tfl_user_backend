import ServiceBase from '../../common/serviceBase'

export default class HelloWorld extends ServiceBase {
  async run () {
    const response = 'Hello World'
    return response
  }
}
