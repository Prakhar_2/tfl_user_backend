import ServiceBase from '../../common/serviceBase'

const constraints = {
  fixtureId: {
    type: 'number',
    presence: true
  }
}

export default class GetEvent extends ServiceBase {
  get constraints () {
    return constraints
  }

  async run () {
    const {
      databaseConnection: {
        Event: EventModel
      },
      sequelizeTransaction
    } = this.context
    const { fixtureId } = this.args

    const event = await EventModel.findOne({
      where: { fixtureId },
      transaction: sequelizeTransaction
    })

    return event
  }
}
