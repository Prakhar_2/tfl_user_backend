import { UserInputError } from 'apollo-server-express'
import moment from 'moment'
import { getClientIp } from 'request-ip'
import ServiceBase from '../../common/serviceBase'
import translate from '../../lib/languageTranslate'
import sendTestUsdt from '../../lib/publisher/sendTestUsdt'

const constraints = {
  blockChainAddress: {
    type: 'string',
    presence: true
  }
}
export default class requestTestUsdt extends ServiceBase {
  get constraints () {
    return constraints
  }

  async run () {
    const {
      databaseConnection: {
        TokenLedger: TokenLedgerModel
      },
      req: {
        headers: { language }
      },
      sequelizeTransaction
    } = this.context
    const { blockChainAddress } = this.args

    const ipAddress = getClientIp(this.context.req)
    const ipAddressExists = await TokenLedgerModel.findOne({
      where: {
        ipAddress
      },
      transaction: sequelizeTransaction
    })

    if (ipAddressExists) {
      if (new Date() <= moment(ipAddressExists.updatedAt).add(24, 'hours')) {
        throw new UserInputError(translate('USDT_REQUEST_EXCEEDED', language))
      } else {
        ipAddressExists.amount += 10
        ipAddressExists.save({ transaction: sequelizeTransaction })
      }
    } else {
      await TokenLedgerModel.create({
        account: blockChainAddress,
        amount: 10,
        ipAddress
      }, {
        transaction: sequelizeTransaction
      })
    }
    await sendTestUsdt(blockChainAddress)
    return { success: true }
  }
}
