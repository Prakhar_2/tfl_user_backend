import ServiceBase from '../../common/serviceBase'

export default class GetSettings extends ServiceBase {
  async run () {
    const {
      databaseConnection: {
        Setting: SettingModel
      },
      sequelizeTransaction
    } = this.context

    const settings = await SettingModel.findAll({
      transaction: sequelizeTransaction
    })

    return { Settings: settings }
  }
}
