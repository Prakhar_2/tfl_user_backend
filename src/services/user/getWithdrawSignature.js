import { WITHDRAW_REQUEST_STATUS, WITHDRAW_REQUEST_EXPIRY_TIME } from '../../common/constants'
import ServiceBase from '../../common/serviceBase'
import { UserInputError } from 'apollo-server'
import translate from '../../lib/languageTranslate'
import moment from 'moment'

const constraints = {
  fixtureId: {
    type: 'string'
  }
}

export default class GetWithdrawSignature extends ServiceBase {
  get constraints () {
    return constraints
  }

  async run () {
    const {
      context: {
        req: { headers: { language } },
        databaseConnection: {
          WithdrawRequest: WithdrawRequestModel
        },
        sequelizeTransaction,
        auth: { id: userId }
      },
      args: { fixtureId }
    } = this

    const withdrawRequest = await WithdrawRequestModel.findOne({
      where: {
        userId,
        fixtureId,
        status: WITHDRAW_REQUEST_STATUS.APPROVED
      },
      transaction: sequelizeTransaction
    })

    if (!withdrawRequest) {
      throw new UserInputError(translate('NO_RECORD_FOUND', language))
    }

    if (moment().diff(new Date(withdrawRequest.timestamp).getTime() + WITHDRAW_REQUEST_EXPIRY_TIME, 'minutes') >= 0) {
      withdrawRequest.status = WITHDRAW_REQUEST_STATUS.EXPIRED
      withdrawRequest.save({ transaction: sequelizeTransaction })
      throw new UserInputError(translate('REQUEST_IS_EXPIRED', language))
    }

    return withdrawRequest
  }
}
