import ServiceBase from '../../common/serviceBase'
import { COMMON_NONCE } from '../../common/constants'

const constraints = {
  userAddress: {
    type: 'string',
    presence: { message: 'User address is required' }
  }
}

export default class CheckUserAddress extends ServiceBase {
  get constraints () {
    return constraints
  }

  async run () {
    const {
      databaseConnection: {
        User: UserModel
      },
      sequelizeTransaction
    } = this.context

    const user = await UserModel.findOne({
      where: {
        blockchainAddress: String(this.args.userAddress).toLowerCase()
      },
      transaction: sequelizeTransaction
    })

    if (!user) {
      return { message: 'PLAYER_NOT_FOUND', exist: false, nonce: COMMON_NONCE }
    }

    return { message: 'PLAYER_FOUND', exist: true, nonce: user.nonce }
  }
}
