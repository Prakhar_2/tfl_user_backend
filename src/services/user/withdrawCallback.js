import { ForbiddenError, UserInputError } from 'apollo-server'
import np from 'number-precision'
import { TRANSACTION_STATUS, TRANSACTION_TYPE, USER_TYPES, WITHDRAW_REQUEST_STATUS } from '../../common/constants'
import ServiceBase from '../../common/serviceBase'
import translate from '../../lib/languageTranslate'
import CreateWallet from './createWallet'

const constraints = {
  fixtureId: {
    type: 'string'
  },
  blockchainAddress: {
    type: 'string'
  }
}

export default class WithdrawCallback extends ServiceBase {
  get constraints () {
    return constraints
  }

  async run () {
    const {
      context: {
        req: { headers: { language } },
        databaseConnection: {
          Bet: BetModel,
          User: UserModel,
          Wallet: WalletModel,
          Ledger: LedgerModel,
          WithdrawRequest: WithdrawRequestModel
        },
        sequelizeTransaction
      },
      args: {
        fixtureId,
        blockchainAddress
      }
    } = this

    const user = await UserModel.findOne({
      where: {
        blockchainAddress: blockchainAddress
      },
      include: {
        model: WalletModel
      },
      transaction: sequelizeTransaction
    })
    if (!user) {
      throw new UserInputError(translate('USER_NOT_FOUND', language))
    }

    let userWallet = user.Wallets[0]
    if (!userWallet) {
      userWallet = await CreateWallet.run({ userId: user.id, userType: USER_TYPES.USER }, this.context)
    }

    const withdrawRequest = await WithdrawRequestModel.findOne({
      where: {
        userId: user.id,
        fixtureId: fixtureId,
        status: WITHDRAW_REQUEST_STATUS.APPROVED
      },
      transaction: sequelizeTransaction
    })
    if (!withdrawRequest) {
      throw new UserInputError(translate('WITHDRAW_REQUEST_NOT_FOUND', language))
    }

    try {
      await LedgerModel.create({
        sourceWalletId: userWallet.id,
        targetWalletId: null,
        amount: withdrawRequest.amount,
        status: TRANSACTION_STATUS.SUCCESS,
        comment: '',
        transactionType: TRANSACTION_TYPE.DEBIT
      }, { transaction: sequelizeTransaction })

      await BetModel.update({
        isCashedOut: true
      }, {
        where: {
          withdrawRequestId: withdrawRequest.id
        },
        transaction: sequelizeTransaction
      })

      withdrawRequest.status = WITHDRAW_REQUEST_STATUS.CONFIRMED
      userWallet.amount = np.minus(userWallet.amount, withdrawRequest.amount)

      await userWallet.save({ transaction: sequelizeTransaction })
      await withdrawRequest.save({ transaction: sequelizeTransaction })
    } catch {
      throw new ForbiddenError(translate('WITHDRAW_APPROVAL_REJECTED', language))
    }
  }
}
