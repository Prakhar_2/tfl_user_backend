import ServiceBase from '../../common/serviceBase'
import translate from '../../lib/languageTranslate'
import { ForbiddenError } from 'apollo-server'

const constraints = {
  userId: {
    type: 'string',
    presence: { message: 'User id is required' }
  },
  userType: {
    type: 'string',
    presence: { message: 'User type is required' }
  }
}

export default class CreateWallet extends ServiceBase {
  get constraints () {
    return constraints
  }

  async run () {
    const {
      req: { headers: { language } },
      databaseConnection: {
        Wallet: WalletModel
      },
      sequelizeTransaction
    } = this.context

    try {
      const wallet = await WalletModel.create({
        userId: this.args.userId,
        userType: this.args.userType
      }, { transaction: sequelizeTransaction })

      return wallet
    } catch {
      throw new ForbiddenError(translate('SOMETHING_WENT_WRONG', language))
    }
  }
}
