import { Op } from 'sequelize'
import { FIXTURE_STATUS, SETTLEMENT } from '../../common/constants'
import ServiceBase from '../../common/serviceBase'
import { sequelize } from '../../db/models'

const constraints = {
  limit: {
    numericality: {
      onlyInteger: true,
      greaterThan: 0
    }
  },
  pageNumber: {
    numericality: {
      onlyInteger: true,
      greaterThan: -1
    }
  }
}

export default class GetFixtureList extends ServiceBase {
  get constraints () {
    return constraints
  }

  async run () {
    const {
      context: {
        databaseConnection: {
          Bet: BetModel,
          Event: EventModel,
          WithdrawRequest: WithdrawRequestModel
        },
        sequelizeTransaction,
        auth: { id: userId }
      },
      args: { limit, pageNumber = 1 }
    } = this

    const offset = (limit || 0) * (pageNumber - 1)

    const bets = await BetModel.findAndCountAll({
      group: ['Bet.event_id', 'Bet.user_id', 'Bet.is_cashed_out', 'Event.id', 'Event.start_date', 'WithdrawRequest.id'],
      attributes: [
        'eventId',
        'userId',
        'isCashedOut'
      ],
      where: {
        userId,
        isCashedOut: false,
        status: { [Op.notIn]: [SETTLEMENT.USER_CANCELLED, SETTLEMENT.PENDING] },
        winningsAfterCommission: { [Op.gt]: 0 }
      },
      include: [{
        model: EventModel,
        where: {
          fixtureStatus: { [Op.notIn]: [FIXTURE_STATUS.IN_PROGRESS, FIXTURE_STATUS.ABOUT_TO_START, FIXTURE_STATUS.NOT_STARTED] },
          modified: {
            [Op.lte]: sequelize.literal("NOW() - INTERVAL '30 MINUTES'")
          }
        },
        required: true
      }, {
        model: WithdrawRequestModel
      }],
      offset,
      limit,
      order: [[sequelize.col('Event.start_date'), 'desc']],
      transaction: sequelizeTransaction
    })

    const queriedCount = bets?.count?.length || 0
    return {
      bets: bets.rows,
      queriedCount,
      numberOfPages: Math.ceil(queriedCount / limit) || 1
    }
  }
}
