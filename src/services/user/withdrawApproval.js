import { ForbiddenError, UserInputError } from 'apollo-server'
import moment from 'moment'
import np from 'number-precision'
import { Op } from 'sequelize'
import { FIXTURE_STATUS, WITHDRAW_REQUEST_EXPIRY_TIME, WITHDRAW_REQUEST_STATUS } from '../../common/constants'
import ServiceBase from '../../common/serviceBase'
import { sequelize } from '../../db/models'
import translate from '../../lib/languageTranslate'
import getMaxWithdrawAmountFixture from '../../lib/publisher/getMaxWithdrawAmountFixture'
import getWithdrawApproval from '../../lib/publisher/getWithdrawApproval'
import GetWithdrawableAmountForAFixture from './getWithdrawableAmountForAFixture'

const constraints = {
  fixtureId: {
    type: 'string'
  }
}

export default class WithdrawApproval extends ServiceBase {
  get constraints () {
    return constraints
  }

  isTimeLessThen (initialTime, time) {
    return moment().diff(new Date(initialTime).getTime() + time, 'minutes') < 0
  }

  async run () {
    const {
      context: {
        req: { headers: { language } },
        databaseConnection: {
          Bet: BetModel,
          User: UserModel,
          Event: EventModel,
          Wallet: WalletModel,
          WithdrawRequest: WithdrawRequestModel
        },
        sequelizeTransaction,
        auth: { id: userId }
      },
      args: { fixtureId }
    } = this

    const user = await UserModel.findOne({
      where: {
        id: userId,
        active: true
      },
      include: {
        model: WalletModel
      },
      transaction: sequelizeTransaction
    })
    if (!user) {
      throw new UserInputError(translate('USER_NOT_FOUND', language))
    }

    const event = await EventModel.findOne({ where: { fixtureId }, transaction: sequelizeTransaction })
    if (!event) {
      throw new UserInputError(translate('EVENT_NOT_FOUND', language))
    }

    const modified = moment(event.modified)
    const now = moment(new Date())

    if (moment.duration(now.diff(modified)).asMinutes() <= 30) {
      throw new UserInputError(translate('EVENT_NOT_SETTLED_TO_WITHDRAW', language))
    }

    if (event.fixtureStatus === FIXTURE_STATUS.IN_PROGRESS || event.fixtureStatus === FIXTURE_STATUS.ABOUT_TO_START || event.fixtureStatus === FIXTURE_STATUS.NOT_STARTED) {
      throw new UserInputError(translate('EVENT_IS_NOT_FINISHED_YET', language))
    }

    const { betIds } = await (await GetWithdrawableAmountForAFixture.execute({ fixtureId }, this.context)).result
    if (!betIds.length) {
      throw new UserInputError(translate('BETS_NOT_FOUND', language))
    }

    const withdrawRequestExists = await WithdrawRequestModel.findOne({
      where: {
        userId,
        fixtureId,
        status: { [Op.or]: [WITHDRAW_REQUEST_STATUS.PENDING, WITHDRAW_REQUEST_STATUS.APPROVED, WITHDRAW_REQUEST_STATUS.CONFIRMED] }
      },
      order: [['created_at', 'desc']],
      include: {
        model: BetModel,
        where: {
          id: { [Op.in]: betIds }
        }
      },
      transaction: sequelizeTransaction
    })

    if (withdrawRequestExists) {
      if (this.isTimeLessThen(withdrawRequestExists.createdAt, 2 * 60000)) {
        throw new ForbiddenError(translate('CAN_NOT_GENERATE_REQUEST_FOR_SAME_FIXTURE_WITHIN_2_MINUTES', language))
      }
      if (withdrawRequestExists.status === WITHDRAW_REQUEST_STATUS.APPROVED && this.isTimeLessThen(withdrawRequestExists.timestamp, WITHDRAW_REQUEST_EXPIRY_TIME)) {
        return withdrawRequestExists
      }

      withdrawRequestExists.status = WITHDRAW_REQUEST_STATUS.EXPIRED
      withdrawRequestExists.save({ transaction: sequelizeTransaction })
    }

    try {
      const withdrawRequest = await WithdrawRequestModel.create({
        userId,
        fixtureId,
        walletId: user.Wallets[0].id
      })

      await BetModel.update({
        withdrawRequestId: withdrawRequest.id
      }, {
        where: {
          id: { [Op.in]: betIds }
        },
        transaction: sequelizeTransaction
      })

      const maxWithdrawAmountFixture = await getMaxWithdrawAmountFixture(user.blockchainAddress, fixtureId, userId)
      const betsData = await BetModel.findOne({
        attributes: [
          [sequelize.fn('SUM', sequelize.col('Bet.winnings')), 'totalWinningAmount'],
          [sequelize.fn('SUM', sequelize.col('Bet.winnings_after_commission')), 'withdrawableAmount']
        ],
        where: {
          withdrawRequestId: withdrawRequest.id
        },
        raw: true,
        transaction: sequelizeTransaction
      })

      const systemFee = np.minus(betsData?.totalWinningAmount, betsData?.withdrawableAmount)

      if (maxWithdrawAmountFixture.message.data.maxAmount > 0 && betsData?.withdrawableAmount > 0) {
        withdrawRequest.systemFee = systemFee
        withdrawRequest.amount = betsData.withdrawableAmount
        const withdrawSignature = await getWithdrawApproval(user.blockchainAddress, fixtureId, betsData.withdrawableAmount, userId, systemFee)

        withdrawRequest.signature = JSON.stringify(withdrawSignature.message.data.signature)
        withdrawRequest.timestamp = np.times(withdrawSignature.message.data.timestamp, 1000)
        withdrawRequest.status = WITHDRAW_REQUEST_STATUS.APPROVED
      } else {
        withdrawRequest.status = WITHDRAW_REQUEST_STATUS.REJECTED
        withdrawRequest.timestamp = new Date().getTime()
      }

      await withdrawRequest.save({ transaction: sequelizeTransaction })
      return withdrawRequest
    } catch {
      throw new ForbiddenError(translate('WITHDRAW_APPROVAL_REJECTED', language))
    }
  }
}
