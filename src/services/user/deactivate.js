import ServiceBase from '../../common/serviceBase'
import { AuthenticationError, ForbiddenError } from 'apollo-server'
import translate from '../../lib/languageTranslate'

export default class Deactivate extends ServiceBase {
  async run () {
    const {
      req: { headers: { language } },
      databaseConnection: {
        User: UserModel
      },
      sequelizeTransaction,
      auth: { id: userId }
    } = this.context

    const user = await UserModel.findOne({
      where: {
        id: userId,
        active: true
      },
      transaction: sequelizeTransaction
    })

    if (!user) {
      throw new AuthenticationError(translate('USER_NOT_FOUND', language))
    }

    try {
      user.active = false
      await user.save({ transaction: sequelizeTransaction })

      return { success: true }
    } catch {
      throw new ForbiddenError(translate('SOMETHING_WENT_WRONG', language))
    }
  }
}
