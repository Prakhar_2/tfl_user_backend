import ServiceBase from '../../common/serviceBase'
import { AuthenticationError, ForbiddenError, UserInputError } from 'apollo-server'
import translate from '../../lib/languageTranslate'
import config from '../../config/app'
import * as jwt from 'jsonwebtoken'
import web3 from '../../lib/web3'
import CreateWallet from './createWallet'
import { USER_TYPES, COMMON_NONCE } from '../../common/constants'
import { v4 as uuidv4 } from 'uuid'

const constraints = {
  signedMessage: {
    type: 'string'
  },
  blockchainAddress: {
    type: 'string'
  }
}

export default class Login extends ServiceBase {
  get constraints () {
    return constraints
  }

  async run () {
    const {
      req: { headers: { language } },
      databaseConnection: {
        User: UserModel
      },
      sequelizeTransaction
    } = this.context

    const blockchainAddress = String(this.args.blockchainAddress).toLowerCase()

    if (!web3.utils.isAddress(blockchainAddress)) {
      throw new AuthenticationError(translate('INVALID_BLOCKCHAIN_ADDRESS', language))
    }

    let user = await UserModel.findOne({
      where: {
        blockchainAddress: blockchainAddress
      },
      transaction: sequelizeTransaction
    })

    if (user && !user.active) {
      throw new UserInputError(translate('ACCOUNT_NOT_ACTIVE', language))
    }

    const recoveredBlockchainAddress = String(await web3.eth.accounts.recover(user?.nonce || COMMON_NONCE, this.args.signedMessage)).toLowerCase()
    if (recoveredBlockchainAddress !== blockchainAddress) {
      throw new UserInputError(translate('GIVEN_ADDRESS_AND_RECOVERED_ADDRESS_MISMATCH', language))
    }

    try {
      if (!user) {
        user = await UserModel.create({
          blockchainAddress: recoveredBlockchainAddress,
          userName: recoveredBlockchainAddress.substr(0, 4) + recoveredBlockchainAddress.substr(recoveredBlockchainAddress.length - 4, recoveredBlockchainAddress.length)
        }, { transaction: sequelizeTransaction })

        await CreateWallet.execute({
          userId: user.id,
          userType: USER_TYPES.USER
        }, this.context)
      } else {
        user.nonce = uuidv4()
        await user.save({ transaction: sequelizeTransaction })
      }

      const accessToken = await jwt.sign({ id: user.id }, config.get('auth.app_secret'), {
        expiresIn: config.get('auth.token_expiry_time')
      })

      return { accessToken, User: user }
    } catch {
      throw new ForbiddenError(translate('SOMETHING_WENT_WRONG', language))
    }
  }
}
