import ServiceBase from '../../common/serviceBase'
import { UserInputError } from 'apollo-server'
import translate from '../../lib/languageTranslate'
import { sequelize } from '../../db/models'
import { SETTLEMENT } from '../../common/constants'

export default class GetUser extends ServiceBase {
  async run () {
    const {
      req: { headers: { language } },
      databaseConnection: {
        Bet: BetModel,
        User: UserModel
      },
      sequelizeTransaction,
      auth: { id: userId }
    } = this.context

    const user = await UserModel.findOne({
      group: ['User.id'],
      where: {
        id: userId,
        active: true
      },
      include: [{
        attributes: [
          [sequelize.fn('COUNT', sequelize.col('Bets.id')), 'totalNumberOfBets']
        ],
        model: BetModel
      }],
      raw: true,
      transaction: sequelizeTransaction
    })

    if (!user) {
      throw new UserInputError(translate('USER_NOT_FOUND', language))
    }

    const betsData = await BetModel.findOne({
      attributes: [
        [sequelize.fn('MAX', sequelize.col('winnings_after_commission')), 'maxBetWinning'],
        [sequelize.fn('SUM', sequelize.col('winnings_after_commission')), 'totalBetWinning']
      ],
      where: {
        userId: user.id,
        status: SETTLEMENT.WINNER
      },
      raw: true
    })

    return {
      User: user,
      maxBetWinning: betsData.maxBetWinning || 0,
      totalBetWinning: betsData.totalBetWinning || 0,
      totalNumberOfBets: user['Bets.totalNumberOfBets']
    }
  }
}
