import { UserInputError } from 'apollo-server-errors'
import np from 'number-precision'
import { Op } from 'sequelize'
import { SETTLEMENT } from '../../common/constants'
import ServiceBase from '../../common/serviceBase'
import { sequelize } from '../../db/models'
import translate from '../../lib/languageTranslate'
import GetUser from './getUser'

const constraints = {
  fixtureId: {
    type: 'string'
  }
}

export default class GetWithdrawableAmountForAFixture extends ServiceBase {
  get constraints () {
    return constraints
  }

  async run () {
    const {
      context: {
        req: { headers: { language } },
        databaseConnection: {
          Bet: BetModel,
          Event: EventModel
        },
        sequelizeTransaction
      },
      args: { fixtureId }
    } = this

    const { User: user } = await (await GetUser.execute({}, this.context)).result

    const event = await EventModel.findOne({ where: { fixtureId }, transaction: sequelizeTransaction })
    if (!event) {
      throw new UserInputError(translate('EVENT_NOT_FOUND', language))
    }

    const betsData = await BetModel.findOne({
      group: ['Bet.event_id'],
      attributes: [
        [sequelize.fn('ARRAY_AGG', sequelize.col('Bet.id')), 'betIds'],
        [sequelize.fn('SUM', sequelize.col('Bet.winnings')), 'totalWinningAmount'],
        [sequelize.fn('SUM', sequelize.col('Bet.winnings_after_commission')), 'totalWinningAmountAfterCommission']
      ],
      where: {
        userId: user.id,
        status: { [Op.notIn]: [SETTLEMENT.USER_CANCELLED, SETTLEMENT.PENDING] },
        eventId: event.id,
        isCashedOut: false,
        winningsAfterCommission: { [Op.gt]: 0 }
      },
      raw: true,
      transaction: sequelizeTransaction
    })

    let systemFee = 0

    if (betsData && betsData.totalWinningAmount > betsData.totalWinningAmountAfterCommission) {
      systemFee = np.minus(betsData.totalWinningAmount, betsData.totalWinningAmountAfterCommission)
    }

    return {
      betIds: betsData?.betIds || [],
      withdrawableAmount: betsData?.totalWinningAmountAfterCommission || 0,
      fixtureId,
      userId: user.id,
      systemFee
    }
  }
}
