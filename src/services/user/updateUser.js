import ServiceBase from '../../common/serviceBase'
import { ForbiddenError, UserInputError } from 'apollo-server'
import translate from '../../lib/languageTranslate'

const constraints = {
  userName: {
    type: 'string',
    length: {
      minimum: 4,
      maximum: 20
    },
    format: {
      pattern: '^[a-zA-Z0-9]*$',
      message: 'Can only contain letters and numbers'
    }
  },
  firstName: {
    type: 'string',
    length: {
      minimum: 3,
      maximum: 20
    },
    format: {
      pattern: '^[a-zA-Z]*$',
      flags: 'i',
      message: 'can only contain a-z and A-Z'
    }
  },
  lastName: {
    type: 'string',
    length: {
      minimum: 3,
      maximum: 20
    },
    format: {
      pattern: '^[a-zA-Z]*$',
      flags: 'i',
      message: 'can only contain a-z and A-Z'
    }
  },
  email: {
    type: 'string',
    email: {
      message: 'Invalid email'
    }
  },
  dateOfBirth: {
    type: 'string',
    format: {
      pattern: /^\d{4}-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$/,
      message: 'invalid date'
    }
  }
}

export default class UpdateUser extends ServiceBase {
  get constraints () {
    return constraints
  }

  async run () {
    const {
      req: { headers: { language } },
      databaseConnection: {
        User: UserModel
      },
      sequelizeTransaction,
      auth: { id: userId }
    } = this.context

    const user = await UserModel.findOne({
      where: {
        id: userId
      },
      transaction: sequelizeTransaction
    })

    if (!user) {
      throw new UserInputError(translate('USER_NOT_FOUND', language))
    } else if (!user.active) {
      throw new UserInputError(translate('ACCOUNT_NOT_ACTIVE', language))
    }

    user.lastName = this.args.lastName || user.lastName
    user.firstName = this.args.firstName || user.firstName
    user.dateOfBirth = this.args.dateOfBirth || user.dateOfBirth

    if (this.args.userName) {
      const userNameCount = await UserModel.count({
        where: {
          userName: this.args.userName
        },
        transaction: sequelizeTransaction
      })

      if (userNameCount) {
        throw new UserInputError(translate('USERNAME_ALREADY_EXISTS', language))
      }

      user.userName = this.args.userName
    }

    if (this.args.email) {
      const emailCount = await UserModel.count({
        where: {
          email: this.args.email
        },
        transaction: sequelizeTransaction
      })

      if (emailCount) {
        throw new UserInputError(translate('EMAIL_ALREADY_EXISTS', language))
      }

      user.email = this.args.email
    }

    try {
      await user.save({ transaction: sequelizeTransaction })
      return user
    } catch {
      throw new ForbiddenError(translate('USER_UPDATION_FAILED', language))
    }
  }
}
