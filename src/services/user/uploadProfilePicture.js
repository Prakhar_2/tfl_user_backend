import { ApolloError } from 'apollo-server-express'
import ServiceBase from '../../common/serviceBase'
import AWS from 'aws-sdk'
import imageUpload from '../../lib/imageUpload'
import fetchAndDeleteById from '../../lib/fetchAndDeleteById'
import { v4 as uuid } from 'uuid'
import config from '../../config/app'
import translate from '../../lib/languageTranslate'

export default class UploadProfilePicture extends ServiceBase {
  async run () {
    const {
      req: { headers: { language } },
      databaseConnection: {
        User: UserModel
      },
      auth: { id: userId },
      sequelizeTransaction
    } = this.context

    const { file } = this.args
    const bucketName = config.get('aws.s3.bucket_name')

    try {
      const awaitedFile = await file.file
      let storedImagePath = {}
      const stream = await awaitedFile.createReadStream()
      const originalName = awaitedFile.filename.split('.')[0]
      let fileName = `${originalName}_${userId}_${uuid()}.${awaitedFile.mimetype.split('/')[1]}`
      fileName = fileName.split(' ').join('')

      // Configuring AWS S3 object
      await AWS.config.update({
        accessKeyId: config.get('aws.s3.access_key_id'),
        secretAccessKey: config.get('aws.s3.secret_access_key'),
        region: config.get('aws.s3.region')
      })

      const bucketPath = `${userId}/${fileName}`
      const s3Bucket = new AWS.S3({ params: { Bucket: bucketName } })

      await fetchAndDeleteById(s3Bucket, bucketName, userId)
      storedImagePath = await imageUpload(s3Bucket, bucketPath, stream)

      if (storedImagePath) {
        const updated = await UserModel.update({
          imageUrl: storedImagePath.Location
        }, {
          where: {
            id: userId
          },
          transaction: sequelizeTransaction
        })

        return {
          uploaded: updated[0] === 1,
          location: storedImagePath.Location
        }
      } else {
        throw new ApolloError(translate('FILE_UPLOAD_FAILED', language), 500)
      }
    } catch (e) {
      throw new ApolloError(translate([e.message] || 'FILE_UPLOAD_FAILED', language), 500)
    }
  }
}
