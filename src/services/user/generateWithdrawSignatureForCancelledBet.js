import { ForbiddenError, UserInputError } from 'apollo-server'
import moment from 'moment'
import np from 'number-precision'
import { Op } from 'sequelize'
import { SETTLEMENT, WITHDRAW_REQUEST_EXPIRY_TIME, WITHDRAW_REQUEST_STATUS } from '../../common/constants'
import ServiceBase from '../../common/serviceBase'
import translate from '../../lib/languageTranslate'
import getMaxWithdrawAmountFixture from '../../lib/publisher/getMaxWithdrawAmountFixture'
import getWithdrawApproval from '../../lib/publisher/getWithdrawApproval'

const constraints = {
  betId: {
    type: 'number',
    presence: { message: 'Bet id is required' }
  }
}

export default class GenerateWithdrawSignatureForCancelledBet extends ServiceBase {
  get constraints () {
    return constraints
  }

  isTimeLessThen (initialTime, time) {
    return moment().diff(new Date(initialTime).getTime() + time * 60000, 'minutes') < 0
  }

  async run () {
    const {
      context: {
        req: { headers: { language } },
        databaseConnection: {
          Bet: BetModel,
          User: UserModel,
          Event: EventModel,
          WithdrawRequest: WithdrawRequestModel
        },
        sequelizeTransaction
      }
    } = this

    const bet = await BetModel.findOne({
      where: {
        id: this.args.betId
      },
      include: [{
        model: WithdrawRequestModel
      }, {
        model: EventModel,
        attributes: ['fixtureId']
      }, {
        model: UserModel,
        attributes: ['id', 'blockchainAddress']
      }],
      transaction: sequelizeTransaction
    })

    if (!bet) {
      throw new UserInputError(translate('BETS_NOT_FOUND', language))
    }

    if (bet.status !== SETTLEMENT.USER_CANCELLED) {
      throw new UserInputError(translate('BET_IS_NOT_CANCELLED', language))
    }

    const user = await UserModel.findOne({
      where: {
        id: bet.userId,
        active: true
      },
      transaction: sequelizeTransaction
    })

    if (!user) {
      throw new UserInputError(translate('USER_NOT_FOUND', language))
    }

    const withdrawRequestExists = bet.WithdrawRequest

    const withdrawRequestExistsForOtherBet = await WithdrawRequestModel.findOne({
      where: {
        userId: bet.userId,
        fixtureId: bet.Event.fixtureId,
        status: { [Op.or]: [WITHDRAW_REQUEST_STATUS.PENDING, WITHDRAW_REQUEST_STATUS.APPROVED, WITHDRAW_REQUEST_STATUS.CONFIRMED] }
      },
      order: [['created_at', 'desc']],
      transaction: sequelizeTransaction
    })

    if (withdrawRequestExists || withdrawRequestExistsForOtherBet) {
      if (withdrawRequestExists?.id === withdrawRequestExistsForOtherBet.id) {
        if (
          (withdrawRequestExists.status === WITHDRAW_REQUEST_STATUS.PENDING && moment().diff(new Date(withdrawRequestExists.createdAt).getTime() + WITHDRAW_REQUEST_EXPIRY_TIME, 'minutes') < 0) ||
        (withdrawRequestExists.status === WITHDRAW_REQUEST_STATUS.APPROVED && moment().diff(new Date(withdrawRequestExists.timestamp).getTime() + WITHDRAW_REQUEST_EXPIRY_TIME, 'minutes') < 0)
        ) {
          return withdrawRequestExists
        }

        withdrawRequestExists.status = WITHDRAW_REQUEST_STATUS.EXPIRED
        withdrawRequestExists.save({ transaction: sequelizeTransaction })
      } else {
        if (this.isTimeLessThen(withdrawRequestExistsForOtherBet.createdAt, 2)) {
          throw new ForbiddenError(translate('WITHDRAW_APPROVAL_REJECTED', language))
        }
      }
    }

    try {
      const fixtureId = bet.Event.fixtureId
      const withdrawRequest = await WithdrawRequestModel.create({
        fixtureId,
        userId: bet.userId,
        walletId: bet.walletId
      })

      bet.withdrawRequestId = withdrawRequest.id
      await bet.save({ transaction: sequelizeTransaction })

      const maxWithdrawAmountFixture = await getMaxWithdrawAmountFixture(user.blockchainAddress, fixtureId, user.id)

      const systemFee = np.minus(bet?.winnings, bet?.winningsAfterCommission)

      if (maxWithdrawAmountFixture.message.data.maxAmount > 0 && bet?.winningsAfterCommission > 0) {
        withdrawRequest.systemFee = systemFee
        withdrawRequest.amount = bet.winningsAfterCommission
        const withdrawSignature = await getWithdrawApproval(user.blockchainAddress, fixtureId, bet.winningsAfterCommission, user.id, systemFee)

        withdrawRequest.signature = JSON.stringify(withdrawSignature.message.data.signature)
        withdrawRequest.timestamp = np.times(withdrawSignature.message.data.timestamp, 1000)
        withdrawRequest.status = WITHDRAW_REQUEST_STATUS.APPROVED
      } else {
        withdrawRequest.status = WITHDRAW_REQUEST_STATUS.REJECTED
        withdrawRequest.timestamp = new Date().getTime()
      }

      await withdrawRequest.save({ transaction: sequelizeTransaction })
      return withdrawRequest
    } catch {
      throw new ForbiddenError(translate('WITHDRAW_APPROVAL_REJECTED', language))
    }
  }
}
