import { ForbiddenError } from 'apollo-server'
import { TRANSACTION_STATUS } from '../../common/constants'
import ServiceBase from '../../common/serviceBase'
import translate from '../../lib/languageTranslate'

const constraints = {
  sourceWalletId: {
    type: 'string'
  },
  targetWalletId: {
    type: 'string',
    presence: { message: 'Target wallet id for credit is required' }
  },
  amount: {
    type: 'number',
    presence: { message: 'Amount is required' }
  },
  transactionType: {
    type: 'number',
    presence: { message: 'Transaction type is required' }
  },
  comment: {
    type: 'string'
  },
  betId: {
    type: 'string'
  }
}

export default class CreateCreditLedgerEntry extends ServiceBase {
  get constraints () {
    return constraints
  }

  async run () {
    const {
      context: {
        req: { headers: { language } },
        databaseConnection: {
          Ledger: LedgerModel,
          Wallet: WalletModel
        },
        sequelizeTransaction
      },
      args: {
        betId,
        amount,
        comment,
        sourceWalletId,
        targetWalletId,
        transactionType
      }
    } = this

    try {
      const ledger = await LedgerModel.create({
        sourceWalletId,
        targetWalletId,
        status: TRANSACTION_STATUS.SUCCESS,
        amount,
        comment: comment || '',
        betId: betId || null,
        transactionType
      }, {
        transaction: sequelizeTransaction
      })

      const wallet = await WalletModel.increment('amount', {
        by: amount,
        where: {
          id: targetWalletId
        },
        transaction: sequelizeTransaction
      })

      return { ledger, wallet }
    } catch (error) {
      throw new ForbiddenError(translate('FAILED_TO_CERATE_LEDGER_ENTRY', language))
    }
  }
}
