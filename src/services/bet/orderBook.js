import { UserInputError } from 'apollo-server'
import np from 'number-precision'
import { BET_TYPE, SETTLEMENT } from '../../common/constants'
import ServiceBase from '../../common/serviceBase'
import { sequelize } from '../../db/models'
import translate from '../../lib/languageTranslate'

const constraints = {
  fixtureId: {
    type: 'number',
    presence: true
  },
  marketId: {
    type: 'number',
    presence: true
  }
}

export default class OrderBook extends ServiceBase {
  get constraints () {
    return constraints
  }

  async run () {
    const {
      context: {
        req: { headers: { language } },
        databaseConnection: {
          Bet: BetModel,
          Event: EventModel,
          Market: MarketModel
        },
        sequelizeTransaction
      },
      args: { fixtureId, marketId }
    } = this

    const market = await MarketModel.findOne({ where: { marketId }, transaction: sequelizeTransaction })
    if (!market) {
      throw new UserInputError(translate('MARKET_NOT_FOUND', language))
    }

    const event = await EventModel.findOne({ where: { fixtureId }, transaction: sequelizeTransaction })
    if (!event) {
      throw new UserInputError(translate('EVENT_NOT_FOUND', language))
    }
    const where = {
      eventId: event.id,
      marketId: market.id,
      status: SETTLEMENT.PENDING
    }

    const bets = await BetModel.findAll({
      attributes: [
        [sequelize.fn('sum', sequelize.col('unmatched_liability')), 'totalUnmatchedLiability'],
        [sequelize.fn('sum', sequelize.col('matched_liability')), 'totalMatchedLiability'],
        'odds',
        'betType',
        'outcomeId'
      ],
      where,
      group: ['outcomeId', 'odds', 'bet_type'],
      order: [['odds', 'desc']],
      raw: true,
      transaction: sequelizeTransaction
    })

    const orderBookObject = {}
    await bets.map(bet => {
      const { outcomeId, odds, totalMatchedLiability, totalUnmatchedLiability, betType } = bet

      orderBookObject[outcomeId] = orderBookObject[outcomeId] || {}
      orderBookObject[outcomeId][odds] = orderBookObject[outcomeId][odds] || {
        layBetsMatched: 0,
        layBetsUnmatched: 0,
        backBetsMatched: 0,
        backBetsUnmatched: 0
      }

      const result = orderBookObject[outcomeId][odds]
      if (betType === BET_TYPE.LAY) {
        result.layBetsMatched = np.plus(totalMatchedLiability, result.layBetsMatched)
        result.layBetsUnmatched = np.plus(totalUnmatchedLiability, result.layBetsUnmatched)
      } else {
        result.backBetsMatched = np.plus(totalMatchedLiability, result.backBetsMatched)
        result.backBetsUnmatched = np.plus(totalUnmatchedLiability, result.backBetsUnmatched)
      }
      orderBookObject[outcomeId][odds] = result
    })

    return orderBookObject
  }
}
