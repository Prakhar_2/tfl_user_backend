import { Op } from 'sequelize'
import ServiceBase from '../../common/serviceBase'

const constraints = {
  limit: {
    numericality: {
      onlyInteger: true,
      greaterThan: 0
    }
  },
  pageNumber: {
    numericality: {
      onlyInteger: true,
      greaterThan: -1
    }
  },
  order: {
    object: {
      fields: {
        id: {
          presence: true,
          type: 'string'
        },
        desc: {
          presence: true,
          type: 'boolean'
        }
      }
    }
  },
  filter: {
    array: {
      fields: {
        key: {
          presence: true,
          type: 'string',
          inclusion: {
            within: ['betType', 'status', 'matchingStatus', 'startDate', 'endDate']
          }
        },
        value: {
          presence: true,
          type: 'string'
        }
      }
    }
  }
}

export default class GetBets extends ServiceBase {
  get constraints () {
    return constraints
  }

  async run () {
    const {
      context: {
        databaseConnection: {
          Bet: BetModel
        },
        sequelizeTransaction,
        auth: { id: userId }
      },
      args: { limit, pageNumber = 1, order, filter }
    } = this

    const orderBy = ['createdAt', 'DESC']
    if (order) {
      orderBy[0] = order.id
      orderBy[1] = order.desc ? 'DESC' : 'ASC'
    }

    const offset = (limit || 0) * (pageNumber - 1)
    const where = { userId }
    if (filter && filter.length) {
      filter.forEach(({ key, value }) => {
        if (key === 'startDate') {
          where.createdAt = { ...where.createdAt, [Op.gte]: value }
        } else if (key === 'endDate') {
          where.createdAt = { ...where.createdAt, [Op.lte]: value }
        } else {
          const tempValue = JSON.parse(value)
          where[key] = { [Op.in]: Array.isArray(tempValue) ? tempValue : [tempValue] }
        }
      })
    }

    const { rows: bets, count: queriedCount } = await BetModel.findAndCountAll({
      where,
      offset,
      limit,
      order: [orderBy],
      transaction: sequelizeTransaction
    })

    return {
      bets,
      queriedCount,
      numberOfPages: Math.ceil(queriedCount / limit) || 1
    }
  }
}
