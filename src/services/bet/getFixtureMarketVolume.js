import ServiceBase from '../../common/serviceBase'
import { sequelize } from '../../db/models'
import { Op } from 'sequelize'

const constraints = {
  filter: {
    array: {
      fields: {
        fixtureId: {
          type: 'number',
          presence: true
        },
        marketId: {
          type: 'number',
          presence: true
        }
      }
    }
  }
}

export default class GetFixtureMarketVolume extends ServiceBase {
  get constraints () {
    return constraints
  }

  async run () {
    const {
      context: {
        databaseConnection: {
          Bet: BetModel,
          Event: EventModel,
          Market: MarketModel
        },
        sequelizeTransaction
      },
      args: { filter }
    } = this

    if (!filter.length) {
      return []
    }

    const marketIds = []
    const fixtureIds = []

    for (const values of filter) {
      marketIds.push(values.marketId)
      fixtureIds.push(values.fixtureId)
    }

    const bets = await BetModel.findAll({
      group: ['Bet.event_id', 'Bet.market_id', 'Bet.outcome_id', 'Bet.outcome_name', 'Bet.odds', 'Bet.bet_type', 'Event.id', 'Market.id'],
      attributes: [
        'eventId',
        'marketId',
        'outcomeId',
        'outcomeName',
        'betType',
        'odds',
        [sequelize.fn('sum', sequelize.col('Bet.matched_liability')), 'totalMatchedLiability'],
        [sequelize.fn('sum', sequelize.col('Bet.unmatched_liability')), 'totalUnmatchedLiability']
      ],
      include: [{
        model: EventModel,
        where: { fixtureId: { [Op.in]: fixtureIds } },
        required: true
      }, {
        model: MarketModel,
        where: { marketId: { [Op.in]: marketIds } },
        required: true
      }],
      raw: true,
      nest: true,
      transaction: sequelizeTransaction
    })

    return bets
  }
}
