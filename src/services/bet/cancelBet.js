import { ForbiddenError, UserInputError } from 'apollo-server'
import { BET_MATCHING_STATUS, LEDGER_COMMENTS, SETTLEMENT, TRANSACTION_TYPE } from '../../common/constants'
import ServiceBase from '../../common/serviceBase'
import translate from '../../lib/languageTranslate'
import CreateCreditLedgerEntry from '../ledger/createCreditLedgerEntry'

const constraints = {
  betId: {
    type: 'number',
    presence: { message: 'Bet id is required' }
  }
}

export default class CancelBet extends ServiceBase {
  get constraints () {
    return constraints
  }

  async run () {
    const {
      context: {
        req: { headers: { language } },
        databaseConnection: {
          Bet: BetModel
        },
        sequelizeTransaction
      },
      args: { betId }
    } = this

    const bet = await BetModel.findOne({ where: { id: betId }, transaction: sequelizeTransaction })

    if (!bet) {
      throw new UserInputError(translate('NO_RECORD_FOUND', language))
    }

    if (bet.matchingStatus !== BET_MATCHING_STATUS.UNMATCHED) {
      throw new UserInputError(translate('CAN_NOT_CANCEL_MATCHED_BET', language))
    }

    if (bet.status !== SETTLEMENT.PENDING) {
      throw new UserInputError(translate('CAN_NOT_CANCEL_SETTLED_BET', language))
    }

    try {
      bet.profit = 0
      bet.winnings = bet.stake
      bet.status = SETTLEMENT.USER_CANCELLED
      bet.winningsAfterCommission = bet.winnings

      await CreateCreditLedgerEntry.run({
        sourceWalletId: null,
        targetWalletId: bet.walletId,
        amount: bet.winningsAfterCommission,
        transactionType: TRANSACTION_TYPE.CREDIT,
        comment: LEDGER_COMMENTS.CREDIT_CANCELLED_BET_AMOUNT_TO_USER,
        betId: bet.id
      }, this.context)

      await bet.save({ transaction: sequelizeTransaction })
      return bet
    } catch {
      throw new ForbiddenError(translate('BET_CANCELLATION_FAILED', language))
    }
  }
}
