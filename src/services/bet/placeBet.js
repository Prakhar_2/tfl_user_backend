import ServiceBase from '../../common/serviceBase'
import { FIXTURE_STATUS } from '../../common/constants'
import translate from '../../lib/languageTranslate'
import { ForbiddenError, UserInputError } from 'apollo-server-express'
import getPlaceBetApproval from '../../lib/publisher/getPlaceBetApproval'

const constraints = {
  fixtureId: {
    type: 'number',
    presence: { message: 'fixture id is required' }
  },
  marketId: {
    type: 'number',
    presence: { message: 'market id is required' }
  },
  outcomeId: {
    type: 'string',
    presence: { message: 'outcome id is required' }
  },
  outcomeName: {
    type: 'string',
    presence: { message: 'outcome name is required' }
  },
  odds: {
    type: 'number',
    presence: { message: 'odds is required' }
  },
  stake: {
    type: 'number',
    presence: { message: 'stake is required' }
  }
}

export default class PlaceBet extends ServiceBase {
  get constraints () {
    return constraints
  }

  async run () {
    const {
      context: {
        req: { headers: { language } },
        databaseConnection: {
          Event: EventModel,
          Market: MarketModel,
          Wallet: WalletModel,
          Setting: SettingModel
        },
        sequelizeTransaction,
        auth: { id: userId }
      },
      args: { fixtureId, marketId, outcomeId, outcomeName, odds, stake }
    } = this

    const event = await EventModel.findOne({ where: { fixtureId }, transaction: sequelizeTransaction })
    if (!event) {
      throw new UserInputError(translate('EVENT_NOT_FOUND', language))
    }

    const market = await MarketModel.findOne({ where: { marketId }, transaction: sequelizeTransaction })
    if (!market) {
      throw new UserInputError(translate('MARKET_NOT_FOUND', language))
    }

    const wallet = await WalletModel.findOne({ where: { userId: userId }, transaction: sequelizeTransaction })

    if (!wallet) {
      throw new UserInputError(translate('WALLET_NOT_FOUND', language))
    }

    if (event.fixtureStatus === FIXTURE_STATUS.FINISHED) {
      throw new UserInputError(translate('EVENT_IS_FINISHED', language))
    }

    const settings = await SettingModel.findAll()
    const setting = new Map() // reducing  complexity
    for (const settingValue of settings) { setting.set(settingValue.dataValues.key, settingValue.dataValues.value) }

    if (setting.has('allow_betting') && setting.get('allow_betting') === 'false') {
      throw new UserInputError(translate('BETTING_NOT_ALLOWED', language))
    }

    if (setting.has('min_odd_amount') && setting.has('max_odd_amount') && (odds < parseInt(setting.get('min_odd_amount')) && odds > setting.get('max_odd_amount'))) {
      throw new UserInputError(translate('ODDS_SHOULD_BE_IN_RANGE', language))
    }

    if (setting.has('min_stake_amount') && stake < parseInt(setting.get('min_stake_amount'))) {
      throw new UserInputError(translate('STAKE_SHOULD_BE_IN_RANGE', language))
    }

    try {
      const outcomeHash = `${outcomeId}-${outcomeName}`
      const data = await getPlaceBetApproval(marketId, fixtureId, userId, outcomeHash)
      return { signature: JSON.stringify(data.message.data.signature), outcomeHash: data.message.data.outcomeHash }
    } catch (e) {
      console.log(e)
      throw new ForbiddenError(translate('PLACE_BET_APPROVAL_REJECTED', language))
    }
  }
}
