import { Op } from 'sequelize'
import { BET_MATCHING_STATUS } from '../../common/constants'
import ServiceBase from '../../common/serviceBase'
import { sequelize } from '../../db/models'

const constraints = {
  fixtureIds: {
    array: true
  }
}

export default class GetFixtureMatchedBetAmount extends ServiceBase {
  get constraints () {
    return constraints
  }

  async run () {
    const {
      context: {
        databaseConnection: {
          Bet: BetModel,
          Event: EventModel,
          Market: MarketModel
        },
        sequelizeTransaction
      },
      args: { fixtureIds }
    } = this

    const bets = await BetModel.findAll({
      group: ['Bet.event_id', 'Bet.market_id', 'Bet.outcome_id', 'Bet.outcome_name', 'Event.id', 'Market.id'],
      attributes: [
        [sequelize.fn('sum', sequelize.col('Bet.matched_liability')), 'totalMatchedLiability'],
        'eventId',
        'marketId',
        'outcomeId',
        'outcomeName'
      ],
      where: {
        matchingStatus: { [Op.in]: [BET_MATCHING_STATUS.FULLY_MATCHED, BET_MATCHING_STATUS.PARTIALLY_MATCHED] }
      },
      include: [{
        model: EventModel,
        where: { fixtureId: { [Op.in]: fixtureIds } }
      }, {
        model: MarketModel
      }],
      raw: true,
      nest: true,
      transaction: sequelizeTransaction
    })

    return bets
  }
}
