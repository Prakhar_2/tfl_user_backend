import ServiceBase from '../../common/serviceBase'

const constraints = {
  limit: {
    numericality: {
      onlyInteger: true,
      greaterThan: 0
    }
  }
}

export default class GetAllBets extends ServiceBase {
  get constraints () {
    return constraints
  }

  async run () {
    const {
      context: {
        databaseConnection: {
          Bet: BetModel
        },
        sequelizeTransaction
      },
      args: { limit = 100 }
    } = this

    const checkedLimit = limit > 100 ? 100 : limit
    const bets = await BetModel.findAll({
      offset: 0,
      limit: checkedLimit,
      order: [['createdAt', 'DESC']],
      transaction: sequelizeTransaction
    })

    return bets
  }
}
