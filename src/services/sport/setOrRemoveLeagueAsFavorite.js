import { UserInputError } from 'apollo-server'
import ServiceBase from '../../common/serviceBase'
import translate from '../../lib/languageTranslate'

const constraints = {
  sportId: {
    type: 'number',
    presence: { message: 'Sport id type is required' }
  },
  leagueId: {
    type: 'number',
    presence: { message: 'League id type is required' }
  }
}

export default class SetOrRemoveLeagueAsFavorite extends ServiceBase {
  get constraints () {
    return constraints
  }

  async run () {
    const {
      req: { headers: { language } },
      databaseConnection: {
        Favorite: FavoriteModel,
        League: LeagueModel,
        Sport: SportModel
      },
      sequelizeTransaction,
      auth: { id: userId }
    } = this.context

    const { sportId, leagueId, isFavorite } = this.args

    const league = await LeagueModel.findOne({
      where: {
        leagueId,
        isDeleted: false
      },
      include: [{
        model: SportModel,
        where: {
          sportId,
          isDeleted: false
        },
        required: true
      }],
      raw: true,
      nest: true,
      transaction: sequelizeTransaction
    })

    if (!league) {
      throw new UserInputError(translate('LEAGUE_NOT_FOUND', language))
    }
    if (!league.Sport) {
      throw new UserInputError(translate('SPORT_NOT_FOUND', language))
    }

    const sport = league.Sport
    const favoriteExists = await FavoriteModel.findOne({
      where: { userId, leagueId: league.id, sportId: sport.id }
    })

    if (favoriteExists) {
      favoriteExists.isFavorite = isFavorite
      await favoriteExists.save({ transaction: sequelizeTransaction })
    } else {
      await FavoriteModel.create({
        userId,
        leagueId: league.id,
        sportId: sport.id,
        isFavorite: true
      }, {
        transaction: sequelizeTransaction
      })
    }

    return { success: true }
  }
}
