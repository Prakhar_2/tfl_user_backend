import { Op } from 'sequelize'
import ServiceBase from '../../common/serviceBase'

const constraints = {
  limit: {
    numericality: {
      onlyInteger: true,
      greaterThan: 0
    }
  },
  pageNumber: {
    numericality: {
      onlyInteger: true,
      greaterThan: -1
    }
  },
  order: {
    object: {
      fields: {
        id: {
          presence: true,
          type: 'string'
        },
        desc: {
          presence: true,
          type: 'boolean'
        }
      }
    }
  },
  filter: {
    array: {
      fields: {
        key: {
          presence: true,
          type: 'string',
          inclusion: {
            within: ['id', 'sportId', 'nameEn']
          }
        },
        value: {
          presence: true,
          type: 'string'
        }
      }
    }
  }
}

export default class GetAllSports extends ServiceBase {
  get constraints () {
    return constraints
  }

  async run () {
    const {
      context: {
        databaseConnection: {
          Sport: SportModel
        },
        sequelizeTransaction
      },
      args: { limit, pageNumber = 1, order, filter }
    } = this

    const orderBy = ['created', 'DESC']
    if (order) {
      orderBy[0] = order.id
      orderBy[1] = order.desc ? 'DESC' : 'ASC'
    }

    const offset = (limit || 0) * (pageNumber - 1)
    const where = { isDeleted: false }
    if (filter && filter.length) {
      filter.forEach(({ key, value }) => {
        if (key === 'nameEn') {
          where[key] = { [Op.iLike]: `%${value}%` }
        } else {
          const tempValue = JSON.parse(value)
          where[key] = { [Op.in]: Array.isArray(tempValue) ? tempValue : [tempValue] }
        }
      })
    }

    const { rows: Sports, count: queriedCount } = await SportModel.findAndCountAll({
      where,
      offset,
      limit,
      order: [orderBy],
      transaction: sequelizeTransaction
    })

    return {
      Sports,
      queriedCount,
      numberOfPages: Math.ceil(queriedCount / limit) || 1
    }
  }
}
