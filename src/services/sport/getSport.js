import { UserInputError } from 'apollo-server'
import { Op } from 'sequelize'
import ServiceBase from '../../common/serviceBase'
import translate from '../../lib/languageTranslate'

const constraints = {
  key: {
    type: 'string',
    presence: { message: 'Filter type is required' }
  },
  value: {
    type: 'string',
    presence: { message: 'Filter value is required' }
  }
}

export default class GetAllSport extends ServiceBase {
  get constraints () {
    return constraints
  }

  async run () {
    const {
      req: { headers: { language } },
      databaseConnection: {
        Sport: SportModel
      },
      sequelizeTransaction
    } = this.context

    const {
      key,
      value
    } = this.args

    const where = {}
    where[key] = key === 'nameEn' ? { [Op.iLike]: `%${value}%` } : value

    const sport = await SportModel.findOne({
      where,
      transaction: sequelizeTransaction
    })

    if (!sport) {
      throw new UserInputError(translate('NO_RECORD_FOUND', language))
    }

    return sport
  }
}
