import { UserInputError } from 'apollo-server'
import ServiceBase from '../../common/serviceBase'
import translate from '../../lib/languageTranslate'

const constraints = {
  sportId: {
    type: 'number',
    presence: { message: 'Sport id type is required' }
  }
}

export default class GetFavoriteLeague extends ServiceBase {
  get constraints () {
    return constraints
  }

  async run () {
    const {
      req: { headers: { language } },
      databaseConnection: {
        Favorite: FavoriteModel,
        League: LeagueModel,
        Sport: SportModel
      },
      sequelizeTransaction,
      auth: { id: userId }
    } = this.context

    const { sportId } = this.args
    const sport = await SportModel.findOne({
      where: {
        sportId,
        isDeleted: false
      },
      transaction: sequelizeTransaction
    })

    if (!sport) {
      throw new UserInputError(translate('SPORT_NOT_FOUND', language))
    }

    const favorite = await FavoriteModel.findAll({
      where: {
        sportId: sport.id,
        userId,
        isFavorite: true
      },
      include: [{
        model: LeagueModel,
        where: {
          isDeleted: false
        },
        required: true
      }],
      transaction: sequelizeTransaction
    })

    if (favorite.length === 0) {
      throw new UserInputError(translate('NO_RECORD_FOUND', language))
    }

    return favorite
  }
}
