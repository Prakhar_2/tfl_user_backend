/**
 * The function is responsible to generate the response for the live ezugi api
 * @param {*} res - object contains all the response params sent to the client
 * @param {*} payload - contains the json object to be sent
 */
export const response = async (res, payload) => {
  res.status(200).json(payload)
}

/**
 * The function is responsible to commit or roll-back the db transaction
 * @param {*} req - object contains all the req params
 * @param {*} failed - contains the boolean flag request failed or passed
 */
export const commitOrRollback = async (req, failed) => {
  if (failed) {
    await req.sequelizeTransaction.rollback()
  } else {
    await req.sequelizeTransaction.commit()
  }
}
