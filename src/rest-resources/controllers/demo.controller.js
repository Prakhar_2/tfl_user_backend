import HelloWorld from '../../services/demo/helloWorld'
import { response } from '../common/helpers'

/**
 * Live ezugi authentication end point for validation of the user
 * @param {*} req - object contains all the request params sent from the client
 * @param {*} res - object contains all the response params sent to the client
 */

export default class DemoController {
  static async helloWorld (req, res) {
    // req.sequelizeTransaction = await sequelize.transaction()
    const { result /**  failed  */ } = await HelloWorld.execute(req.body, req)
    // await commitOrRollback(req, failed)
    return response(res, result)
  }
}
