import express from 'express'
import DemoController from '../../../controllers/demo.controller'

const demoRoutes = express.Router()

demoRoutes.route('/hello').get(DemoController.helloWorld)

export default demoRoutes
