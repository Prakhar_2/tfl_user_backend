import { sign, verify } from 'jsonwebtoken'
import config from '../../config/app'

export const errorChecks = {
  insufficientFundsCheck: (user, amount, responseObject) => {
    if ((user.Wallet.amount + user.Wallet.nonCashAmount) < amount) {
      responseObject.errorCode = 3
      responseObject.errorDescription = 'Insufficient funds'

      return false
    }
    return true
  },

  tokenNotFoundCheck: async (ezugiCredentials, token, responseObject) => {
    let payload
    const authConfig = config.getProperties().auth
    try {
      payload = await verify(token, ezugiCredentials.APP_EZUGI_HASH_KEY)
    } catch (e) {
      responseObject.errorCode = '6'
      responseObject.errorDescription = 'Token not found'

      return false
    }

    payload.newToken = await sign({ id: payload.id }, ezugiCredentials.APP_EZUGI_HASH_KEY, { expiresIn: authConfig.live_casino_token_expiry_time })

    return payload
  },
  userNotFoundCheck: async (context, tokenVerificationData, responseObject) => {
    const WalletModel = context.databaseConnection.Wallet
    const CurrencyModel = context.databaseConnection.Currency

    const user = await context.databaseConnection.User.findOne({
      where: { id: tokenVerificationData.id },
      include: [{
        model: WalletModel,
        include: {
          model: CurrencyModel
        }
      }]
    })

    if (!user) {
      responseObject.errorCode = 7
      responseObject.errorDescription = 'User not found'

      return false
    }

    return user
  },
  userDocumentCheck: async (context, tokenVerificationData, responseObject) => {
    const user = await context.databaseConnection.UserDocument.findOne({
      where: { userId: tokenVerificationData.id, status: 'rejected' }
    })
    if (user) {
      responseObject.errorCode = 7
      responseObject.errorDescription = 'Document not verified'

      return false
    }

    return user
  },

  transactionNotFoundCheck: async (context, transactionId, responseObject) => {
    const transaction = await context.databaseConnection.Transaction.findOne({ where: { transactionId } })

    if (!transaction) {
      responseObject.errorCode = 9
      responseObject.errorDescription = 'Transaction Not Found'

      return false
    }

    return transaction
  },

  negativeAmountCheck: async (amount, responseObject) => {
    if (amount < 0) {
      responseObject.errorCode = 1
      responseObject.errorDescription = 'Negative amount'

      return false
    }

    return true
  }

}
