/**
 * The function is responsible to commit or roll-back the db transaction
 * @param {*} context - object with sequelize transaction
 * @param {*} failed - contains the boolean flag request failed or passed
 */
export const commitOrRollback = async (context, result) => {
  if (result.failed) {
    await context.sequelizeTransaction.rollback()
    throw result.errors
  }
  await context.sequelizeTransaction.commit()
  return 0
}
