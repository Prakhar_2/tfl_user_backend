import validate from 'validate.js'

validate.validators.array = function (value, options, key, attributes) {
  if (options === true) {
    return value && Array.isArray(value) ? null : 'Array is required'
  }

  if (!options.presence && !value) return null

  if (Array.isArray(value)) {
    if (options.fields) {
      for (const val of value) {
        const validationResponse = validate(val, options.fields)
        if (validationResponse) {
          return validationResponse
        }
      }
    }
    return null
  }

  return 'Array is required'
}

validate.validators.object = function (value, options, key, attributes) {
  if (options === true) {
    return value && typeof value === 'object' ? null : 'Object is required'
  }

  if (!options.presence && !value) return null

  if (typeof value === 'object') {
    if (options.fields) {
      const validationResponse = validate(value, options.fields)
      if (validationResponse) {
        return validationResponse
      }
    }
    return null
  }

  return 'Object is required'
}

export default validate
