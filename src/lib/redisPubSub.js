import config from '../config/app'

const { RedisPubSub } = require('graphql-redis-subscriptions')

const connection = {
  host: config.get('pub_sub_redis_db.host'),
  port: config.get('pub_sub_redis_db.port'),
  password: config.get('pub_sub_redis_db.password'),
  connectTimeout: 20000
}

const redisPubSub = new RedisPubSub({ connection })

export default redisPubSub
