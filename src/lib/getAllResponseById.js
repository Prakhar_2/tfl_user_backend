const s3GetResponse = (s3Bucket, bucketName, userId) => {
  const params = {
    Bucket: bucketName,
    Prefix: `${userId}`
  }

  return new Promise((resolve, reject) => {
    s3Bucket.listObjectsV2(params, (err, data) => {
      if (err) {
        reject(err)
      } else {
        resolve(data)
      }
    })
  })
}

export default s3GetResponse
