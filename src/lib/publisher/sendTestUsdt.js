import { CLIENT } from '../../common/constants'
import config from '../../config/app'
import { redisPub } from '../blockchainRedisDB'
import { machineId } from 'node-machine-id'

export default async (address) => {
  const id = await machineId()

  const message = {
    id: `${id} ${address}`,
    blockchain: 'polygon',
    cmd: CLIENT.REDIS_PUB_SUB_CMD.SEND_TEST_USDT,
    address,
    smartContract: config.get('blockchain.smart_contract_address')
  }

  redisPub.publish(CLIENT.REDIS_PUB_CHANNELS.SM_REQUEST, JSON.stringify(message))
}
