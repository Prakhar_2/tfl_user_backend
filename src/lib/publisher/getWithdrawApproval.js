import { CLIENT } from '../../common/constants'
import config from '../../config/app'
import { redisPub, redisSub } from '../blockchainRedisDB'

export default async (address, fixtureId, amount, userId, systemFee) => {
  const localMachineId = Date.now()
  const id = `${localMachineId} ${userId}/${fixtureId}`

  const message = {
    id,
    blockchain: 'bsc',
    cmd: CLIENT.REDIS_PUB_SUB_CMD.GET_WITHDRAW_APPROVAL,
    fixtureId,
    account: address,
    amount,
    systemFee,
    smartContract: config.get('blockchain.smart_contract_address')
  }

  return new Promise((resolve, reject) => {
    redisPub.publish(CLIENT.REDIS_PUB_CHANNELS.SM_REQUEST, JSON.stringify(message))

    const timerId = setTimeout(() => {
      redisSub.removeListener('message', callback)
      reject(Error('Timeout'))
    }, 20000)

    function callback (channel, message) {
      const msg = JSON.parse(message)

      if (msg.cmd === CLIENT.REDIS_PUB_SUB_CMD.GET_WITHDRAW_APPROVAL && msg.id === id) {
        clearTimeout(timerId)
        resolve({ message: msg })
        redisSub.removeListener('message', callback)
      }
    }

    redisSub.on('message', callback)
  })
}
