import Web3 from 'web3'
import config from '../config/app'

const httpOptions = {
  keepAlive: true,
  withCredentials: false,
  timeout: 20000
}

const web3 = new Web3(new Web3.providers.HttpProvider(config.get('blockchain.http_endpoint'), httpOptions))

web3.eth.handleRevert = true
web3.eth.transactionConfirmationBlocks = config.get('blockchain.confirmation_blocks')

export default web3
