import config from '../config/app'
import Redis from 'ioredis'

const connection = {
  host: config.get('blockchain.redis_db.host'),
  port: config.get('blockchain.redis_db.port'),
  db: config.get('blockchain.redis_db.number'),
  password: config.get('blockchain.redis_db.password'),
  retryStrategy (times) {
    return 0
  },
  reconnectOnError () {
    return true
  }
}

if (config.get('blockchain.redis_db.tls') === 'true') {
  connection.tls = {
    host: config.get('blockchain.redis_db.host'),
    rejectUnauthorized: false,
    requestCert: false
  }
}

export const redisPub = new Redis(connection)
export const redisSub = new Redis(connection)
