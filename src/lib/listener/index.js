import AttachMessageListener from './AttachMessageListener'
import { redisSub } from '../blockchainRedisDB'

(async () => {
  try {
    redisSub.on('connect', () => {
      console.log('Redis connected!')
    })

    redisSub.subscribe('smResponse')
    redisSub.subscribe('smNotification')

    await AttachMessageListener.run()
  } catch (err) {
    console.log(err)
  }
})()
