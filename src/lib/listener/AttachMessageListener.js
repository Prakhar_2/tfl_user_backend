import { CLIENT } from '../../common/constants'
import ServiceBase from '../../common/serviceBase'
import databaseConnection, { sequelize } from '../../db/models'
import WithdrawCallback from '../../services/user/withdrawCallback'
import { redisSub } from '../blockchainRedisDB'

export default class AttachMessageListener extends ServiceBase {
  async run () {
    redisSub.on('message', async (channel, message) => {
      const msg = JSON.parse(message)
      if (msg.cmd === CLIENT.REDIS_PUB_SUB_CMD.NOTIFICATION && msg?.data?.values?.systemFee) {
        const context = {
          databaseConnection,
          req: {
            headers: {}
          },
          sequelizeTransaction: await sequelize.transaction()
        }

        try {
          await WithdrawCallback.execute({
            fixtureId: msg.data.values.fixtureId,
            blockchainAddress: msg.data.values.account.toLowerCase()
          }, context)
          await context.sequelizeTransaction.commit()
        } catch (error) {
          await context.sequelizeTransaction.rollback()
          console.log(error)
        }
      }
    })
  }
}
