const imageUpload = (s3Bucket, path, stream) => {
  const config = {
    Key: path,
    Body: '',
    ACL: 'public-read'
  }
  stream.on('error', (error) => console.error(error))
  config.Body = stream

  return new Promise((resolve, reject) => {
    s3Bucket.upload(config, (err, result) => {
      if (err) {
        reject(err)
      } else {
        resolve(result)
      }
    })
  })
}

export default imageUpload
