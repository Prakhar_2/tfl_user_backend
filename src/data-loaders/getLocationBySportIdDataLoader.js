import dataloaderSort from 'dataloader-sort'
import { Op } from 'sequelize'
import ServiceBase from '../common/serviceBase'

export default class GetLocationBySportIdDataLoader extends ServiceBase {
  async run () {
    const {
      databaseConnection: {
        Sport: SportModel,
        Location: LocationModel
      }
    } = this.context

    const sports = await SportModel.findAll({
      where: {
        isDeleted: false,
        id: { [Op.in]: this.args }
      },
      include: {
        model: LocationModel
      }
    })

    const sportsSorted = dataloaderSort(this.args, sports, 'id')
    return sportsSorted.map((sport) => sport ? sport.Locations : [])
  }
}
