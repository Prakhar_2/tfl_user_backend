import dataloaderSort from 'dataloader-sort'
import { Op } from 'sequelize'
import ServiceBase from '../common/serviceBase'

export default class GetSportBySportIdDataLoader extends ServiceBase {
  async run () {
    const {
      databaseConnection: {
        Sport: SportModel
      }
    } = this.context

    const SportData = await SportModel.findAll({
      where: {
        id: { [Op.in]: this.args }
      },
      raw: true
    })

    const SportDataSorted = dataloaderSort(this.args, SportData, 'id')
    return SportDataSorted
  }
}
