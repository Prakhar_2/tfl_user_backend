import dataloaderSort from 'dataloader-sort'
import { Op } from 'sequelize'
import ServiceBase from '../common/serviceBase'

export default class GetLeagueByLeagueIdDataLoader extends ServiceBase {
  async run () {
    const {
      databaseConnection: {
        League: LeagueModel
      }
    } = this.context

    const leagueData = await LeagueModel.findAll({
      where: {
        id: { [Op.in]: this.args }
      },
      raw: true
    })

    const leagueDataSorted = dataloaderSort(this.args, leagueData, 'id')
    return leagueDataSorted
  }
}
