import dataloaderSort from 'dataloader-sort'
import { Op } from 'sequelize'
import ServiceBase from '../common/serviceBase'

export default class GetParticipantsByParticipantIdDataLoader extends ServiceBase {
  async run () {
    const {
      databaseConnection: {
        Participant: ParticipantModel
      }
    } = this.context

    const participantsData = await ParticipantModel.findAll({
      where: {
        id: { [Op.in]: this.args },
        isDeleted: false
      },
      raw: true
    })

    const participantsDataSorted = dataloaderSort(this.args, participantsData, 'id')
    return participantsDataSorted
  }
}
