import dataloaderSort from 'dataloader-sort'
import { Op } from 'sequelize'
import ServiceBase from '../common/serviceBase'

export default class GetEventByEventIdDataLoader extends ServiceBase {
  async run () {
    const {
      databaseConnection: {
        Event: EventModel
      }
    } = this.context

    const eventData = await EventModel.findAll({
      where: {
        id: { [Op.in]: this.args }
      },
      raw: true
    })

    const eventDataSorted = dataloaderSort(this.args, eventData, 'id')
    return eventDataSorted
  }
}
