import dataloaderSort from 'dataloader-sort'
import { Op } from 'sequelize'
import ServiceBase from '../common/serviceBase'

export default class GetUserByUserIdDataLoader extends ServiceBase {
  async run () {
    const {
      databaseConnection: {
        User: UserModel
      }
    } = this.context

    const userData = await UserModel.findAll({
      where: {
        id: { [Op.in]: this.args }
      },
      raw: true
    })

    const userDataSorted = dataloaderSort(this.args, userData, 'id')
    return userDataSorted
  }
}
