import dataloaderSort from 'dataloader-sort'
import { Op } from 'sequelize'
import ServiceBase from '../common/serviceBase'

export default class GetWalletByWalletIdDataLoader extends ServiceBase {
  async run () {
    const {
      databaseConnection: {
        Wallet: WalletModel
      }
    } = this.context

    const walletData = await WalletModel.findAll({
      where: {
        id: { [Op.in]: this.args }
      },
      raw: true
    })

    const walletDataSorted = dataloaderSort(this.args, walletData, 'id')
    return walletDataSorted
  }
}
