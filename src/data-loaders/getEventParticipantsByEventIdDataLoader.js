import dataloaderSort from 'dataloader-sort'
import { Op } from 'sequelize'
import ServiceBase from '../common/serviceBase'

export default class GetEventParticipantsByEventIdDataLoader extends ServiceBase {
  async run () {
    const {
      databaseConnection: {
        Event: EventModel,
        EventParticipant: EventParticipantModel
      }
    } = this.context

    const eventsData = await EventModel.findAll({
      where: {
        id: { [Op.in]: this.args }
      },
      include: {
        model: EventParticipantModel,
        where: {
          isDeleted: false
        }
      }
    })

    const eventsDataSorted = dataloaderSort(this.args, eventsData, 'id')
    return eventsDataSorted.map(event => event ? event.EventParticipants : [])
  }
}
