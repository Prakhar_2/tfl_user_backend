import dataloaderSort from 'dataloader-sort'
import { Op } from 'sequelize'
import { WITHDRAW_REQUEST_STATUS } from '../common/constants'
import ServiceBase from '../common/serviceBase'

export default class GetWithdrawRequestByUserIdAndFixtureIdDataLoader extends ServiceBase {
  async run () {
    const {
      databaseConnection: {
        WithdrawRequest: WithdrawRequestModel
      }
    } = this.context

    const userId = this.args[0].userId
    const fixtureIds = this.args.map(arg => arg.fixtureId)

    const withdrawRequestData = await WithdrawRequestModel.findAll({
      where: {
        userId,
        status: { [Op.in]: [WITHDRAW_REQUEST_STATUS.APPROVED, WITHDRAW_REQUEST_STATUS.PENDING] },
        fixtureId: { [Op.in]: fixtureIds }
      },
      raw: true
    })

    const withdrawRequestSorted = dataloaderSort(fixtureIds, withdrawRequestData, 'fixtureId')
    return withdrawRequestSorted
  }
}
