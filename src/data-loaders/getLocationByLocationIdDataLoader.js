import dataloaderSort from 'dataloader-sort'
import { Op } from 'sequelize'
import ServiceBase from '../common/serviceBase'

export default class GetLocationByLocationIdDataLoader extends ServiceBase {
  async run () {
    const {
      databaseConnection: {
        Location: LocationModel
      }
    } = this.context

    const LocationData = await LocationModel.findAll({
      where: {
        id: { [Op.in]: this.args }
      },
      raw: true
    })

    const LocationDataSorted = dataloaderSort(this.args, LocationData, 'id')
    return LocationDataSorted
  }
}
