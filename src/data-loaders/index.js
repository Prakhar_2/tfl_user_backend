import DataLoader from 'dataloader'
import GetEventByEventIdDataLoader from './getEventByEventIdDataLoader'
import GetEventByFixtureIdDataLoader from './getEventByFixtureIdDataLoader'
import GetEventParticipantsByEventIdDataLoader from './getEventParticipantsByEventIdDataLoader'
import GetLeagueByLeagueIdDataLoader from './getLeagueByLeagueIdDataLoader'
import GetLeagueBySportIdDataLoader from './getLeagueBySportIdDataLoader'
import GetLocationByLocationIdDataLoader from './getLocationByLocationIdDataLoader'
import GetLocationBySportIdDataLoader from './getLocationBySportIdDataLoader'
import GetMarketByMarketIdDataLoader from './getMarketByMarketIdDataLoader'
import GetParticipantsByParticipantIdDataLoader from './getParticipantsByParticipantIdDataLoader'
import GetSportBySportIdDataLoader from './getSportBySportIdDataLoader'
import GetUserByUserIdDataLoader from './getUserByUserIdDataLoader'
import GetUserWalletByUserIdDataLoader from './getUserWalletByUserIdDataLoader'
import GetWalletByWalletIdDataLoader from './getWalletByWalletIdDataLoader'
import GetWithdrawRequestByUserIdAndFixtureIdDataLoader from './getWithdrawRequestByUserIdAndFixtureIdDataLoader'
import GetWithdrawRequestByWithdrawRequestId from './getWithdrawRequestByWithdrawRequestId'

export default (databaseConnection) => {
  const getUserWalletByUserIdDataLoader = new DataLoader(async (keys) => {
    const result = await GetUserWalletByUserIdDataLoader.execute(keys, { databaseConnection })
    return result.result
  })

  const getParticipantsByParticipantIdDataLoader = new DataLoader(async (keys) => {
    const result = await GetParticipantsByParticipantIdDataLoader.execute(keys, { databaseConnection })
    return result.result
  })

  const getEventParticipantsByEventIdDataLoader = new DataLoader(async (keys) => {
    const result = await GetEventParticipantsByEventIdDataLoader.execute(keys, { databaseConnection })
    return result.result
  })

  const getLocationByLocationIdDataLoader = new DataLoader(async (keys) => {
    const result = await GetLocationByLocationIdDataLoader.execute(keys, { databaseConnection })
    return result.result
  })

  const getSportBySportIdDataLoader = new DataLoader(async (keys) => {
    const result = await GetSportBySportIdDataLoader.execute(keys, { databaseConnection })
    return result.result
  })

  const getLeagueByLeagueIdDataLoader = new DataLoader(async (keys) => {
    const result = await GetLeagueByLeagueIdDataLoader.execute(keys, { databaseConnection })
    return result.result
  })

  const getLocationBySportIdDataLoader = new DataLoader(async (keys) => {
    const result = await GetLocationBySportIdDataLoader.execute(keys, { databaseConnection })
    return result.result
  })

  const getLeagueBySportIdDataLoader = new DataLoader(async (keys) => {
    const result = await GetLeagueBySportIdDataLoader.execute(keys, { databaseConnection })
    return result.result
  })

  const getMarketByMarketIdDataLoader = new DataLoader(async (keys) => {
    const result = await GetMarketByMarketIdDataLoader.execute(keys, { databaseConnection })
    return result.result
  })

  const getUserByUserIdDataLoader = new DataLoader(async (keys) => {
    const result = await GetUserByUserIdDataLoader.execute(keys, { databaseConnection })
    return result.result
  })

  const getWalletByWalletIdDataLoader = new DataLoader(async (keys) => {
    const result = await GetWalletByWalletIdDataLoader.execute(keys, { databaseConnection })
    return result.result
  })

  const getEventByEventIdDataLoader = new DataLoader(async (keys) => {
    const result = await GetEventByEventIdDataLoader.execute(keys, { databaseConnection })
    return result.result
  })

  const getEventByFixtureIdDataLoader = new DataLoader(async (keys) => {
    const result = await GetEventByFixtureIdDataLoader.execute(keys, { databaseConnection })
    return result.result
  })

  const getWithdrawRequestByUserIdAndFixtureIdDataLoader = new DataLoader(async (keys) => {
    const result = await GetWithdrawRequestByUserIdAndFixtureIdDataLoader.execute(keys, { databaseConnection })
    return result.result
  })

  const getWithdrawRequestByWithdrawRequestId = new DataLoader(async (keys) => {
    const result = await GetWithdrawRequestByWithdrawRequestId.execute(keys, { databaseConnection })
    return result.result
  })
  return {
    getLeagueBySportIdDataLoader,
    getLocationBySportIdDataLoader,
    getUserWalletByUserIdDataLoader,
    getMarketByMarketIdDataLoader,
    getUserByUserIdDataLoader,
    getWalletByWalletIdDataLoader,
    getEventByEventIdDataLoader,
    getLeagueByLeagueIdDataLoader,
    getLocationByLocationIdDataLoader,
    getSportBySportIdDataLoader,
    getEventParticipantsByEventIdDataLoader,
    getParticipantsByParticipantIdDataLoader,
    getEventByFixtureIdDataLoader,
    getWithdrawRequestByUserIdAndFixtureIdDataLoader,
    getWithdrawRequestByWithdrawRequestId
  }
}
