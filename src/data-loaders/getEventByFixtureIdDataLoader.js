import dataloaderSort from 'dataloader-sort'
import { Op } from 'sequelize'
import ServiceBase from '../common/serviceBase'

export default class GetEventByFixtureIdDataLoader extends ServiceBase {
  async run () {
    const {
      databaseConnection: {
        Event: EventModel
      }
    } = this.context

    const eventData = await EventModel.findAll({
      where: {
        fixtureId: { [Op.in]: this.args }
      },
      raw: true
    })

    const eventDataSorted = dataloaderSort(this.args, eventData, 'fixtureId')
    return eventDataSorted
  }
}
