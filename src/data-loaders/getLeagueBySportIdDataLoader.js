import dataloaderSort from 'dataloader-sort'
import { Op } from 'sequelize'
import { FIXTURE_STATUS } from '../common/constants'
import ServiceBase from '../common/serviceBase'

export default class GetLeagueBySportIdDataLoader extends ServiceBase {
  async run () {
    const {
      databaseConnection: {
        Sport: SportModel,
        Event: EventModel,
        League: LeagueModel
      }
    } = this.context

    const sports = await SportModel.findAll({
      where: {
        isDeleted: false,
        id: { [Op.in]: this.args }
      },
      include: {
        model: LeagueModel,
        where: {
          isDeleted: false,
          leagueId: { [Op.ne]: null }
        },
        include: {
          model: EventModel,
          where: {
            fixtureStatus: {
              [Op.in]: [FIXTURE_STATUS.IN_PROGRESS, FIXTURE_STATUS.NOT_STARTED]
            }
          },
          required: true
        },
        required: false
      }
    })

    const sportsSorted = dataloaderSort(this.args, sports, 'id')
    return sportsSorted.map((sport) => sport ? sport.Leagues : [])
  }
}
