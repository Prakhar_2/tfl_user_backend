import dataloaderSort from 'dataloader-sort'
import { Op } from 'sequelize'
import { WITHDRAW_REQUEST_STATUS } from '../common/constants'
import ServiceBase from '../common/serviceBase'

export default class GetWithdrawRequestByWithdrawRequestId extends ServiceBase {
  async run () {
    const {
      databaseConnection: {
        WithdrawRequest: WithdrawRequestModel
      }
    } = this.context

    const withdrawRequestData = await WithdrawRequestModel.findAll({
      where: {
        id: this.args,
        status: { [Op.in]: [WITHDRAW_REQUEST_STATUS.APPROVED, WITHDRAW_REQUEST_STATUS.PENDING] }
      },
      raw: true
    })

    const withdrawRequestSorted = dataloaderSort(this.args, withdrawRequestData, 'id')
    return withdrawRequestSorted
  }
}
