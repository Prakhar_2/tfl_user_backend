import dataloaderSort from 'dataloader-sort'
import { Op } from 'sequelize'
import { USER_TYPES } from '../common/constants'
import ServiceBase from '../common/serviceBase'

export default class GetUserWalletByUserIdDataLoader extends ServiceBase {
  async run () {
    const {
      databaseConnection: {
        Wallet: WalletModel
      }
    } = this.context

    const userWalletData = await WalletModel.findAll({
      where: {
        userType: USER_TYPES.USER,
        userId: { [Op.in]: this.args }
      },
      raw: true
    })

    const userWalletDataSorted = dataloaderSort(this.args, userWalletData, 'userId')
    return userWalletDataSorted
  }
}
