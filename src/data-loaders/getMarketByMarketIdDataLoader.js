import dataloaderSort from 'dataloader-sort'
import { Op } from 'sequelize'
import ServiceBase from '../common/serviceBase'

export default class GetMarketByMarketIdDataLoader extends ServiceBase {
  async run () {
    const {
      databaseConnection: {
        Market: MarketModel
      }
    } = this.context

    const marketData = await MarketModel.findAll({
      where: {
        id: { [Op.in]: this.args }
      },
      raw: true
    })

    const marketDataSorted = dataloaderSort(this.args, marketData, 'id')
    return marketDataSorted
  }
}
